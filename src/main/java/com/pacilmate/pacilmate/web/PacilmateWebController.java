package com.pacilmate.pacilmate.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PacilmateWebController {

    @GetMapping("")
    public String landingPage() {
        return "web/index";
    }
    
}
