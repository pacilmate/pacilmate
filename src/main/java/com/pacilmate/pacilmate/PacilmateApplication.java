package com.pacilmate.pacilmate;

import com.pacilmate.pacilmate.jda.PacilmateJdaService;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableConfigurationProperties
@EnableAdminServer
@EnableAsync
@EnableScheduling
public class PacilmateApplication implements CommandLineRunner {

    private final PacilmateJdaService pacilmateJdaService;

    public PacilmateApplication(PacilmateJdaService pacilmateJdaService) {
        this.pacilmateJdaService = pacilmateJdaService;
    }

    public static void main(String[] args) {
        SpringApplication.run(PacilmateApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        pacilmateJdaService.startBot();
    }
}
