package com.pacilmate.pacilmate.jda;

import com.pacilmate.pacilmate.feature.builder.BuilderListener;
import com.pacilmate.pacilmate.feature.edit.EditListener;
import com.pacilmate.pacilmate.feature.help.HelpListener;
import com.pacilmate.pacilmate.feature.judge.JudgeListener;
import com.pacilmate.pacilmate.feature.lookup.global.LookupGlobalListener;
import com.pacilmate.pacilmate.feature.lookup.query.CalendarLookupListener;
import com.pacilmate.pacilmate.feature.lookup.specific.LookupSpecificListener;
import com.pacilmate.pacilmate.feature.manage.calendar.ManageCalendarListener;
import com.pacilmate.pacilmate.feature.manage.event.ManageEventListener;
import com.pacilmate.pacilmate.feature.notify.NotifierListener;
import com.pacilmate.pacilmate.feature.pingpong.PingPongListener;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PacilmateJdaServiceImpl implements PacilmateJdaService {

    @Value("${jda.discord.token}")
    private String botToken;
    private JDA jda;

    @Autowired
    private BuilderListener builderListener;

    @Autowired
    private ManageEventListener manageEventListener;

    @Autowired
    private PingPongListener pingPongListener;

    @Autowired
    private LookupGlobalListener lookupGlobalListener;

    @Autowired
    private ManageCalendarListener manageCalendarListener;

    @Autowired
    private CalendarLookupListener calendarLookupListener;

    @Autowired
    private LookupSpecificListener lookupSpecificListener;

    @Autowired
    private HelpListener helpListener;

    @Autowired
    private EditListener editListener;

    @Autowired
    private JudgeListener judgeListener;

    @Autowired
    private NotifierListener notifierListener;

    @Override
    public void startBot() throws InterruptedException, LoginException {
        jda = JDABuilder.createDefault(botToken).build();
        jda.addEventListener(builderListener);
        jda.addEventListener(manageEventListener);
        jda.addEventListener(pingPongListener);
        jda.addEventListener(lookupGlobalListener);
        jda.addEventListener(calendarLookupListener);
        jda.addEventListener(manageCalendarListener);
        jda.addEventListener(lookupSpecificListener);
        jda.addEventListener(helpListener);
        jda.addEventListener(editListener);
        jda.addEventListener(judgeListener);
        jda.addEventListener(notifierListener);
        jda.awaitReady();
    }

    @Override
    public JDA getjda() {
        return jda;
    }
}