package com.pacilmate.pacilmate.jda;

import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;

public interface PacilmateJdaService {
    void startBot() throws InterruptedException, LoginException;

    JDA getjda();
}
