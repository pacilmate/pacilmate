package com.pacilmate.pacilmate.repository;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacilmateCalendarRepository extends JpaRepository<PacilmateCalendar, Integer> {
    PacilmateCalendar findByCalendarId(Integer id);
}
