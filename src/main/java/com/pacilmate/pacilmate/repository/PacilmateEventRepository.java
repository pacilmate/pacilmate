package com.pacilmate.pacilmate.repository;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacilmateEventRepository extends JpaRepository<PacilmateEvent, Integer> {
    PacilmateEvent findByEventId(Integer id);
}
