package com.pacilmate.pacilmate.repository;

import com.pacilmate.pacilmate.model.PacilmateUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacilmateUserRepository extends JpaRepository<PacilmateUser, Integer> {
    PacilmateUser findByUserId(long id);
}
