package com.pacilmate.pacilmate.feature.pingpong;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import java.time.Duration;
import java.time.OffsetDateTime;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class PingPongListener extends ListenerAdapter {

    @Autowired
    PacilmateUtility pacilmateUtility;

    public void pongAccepted(Message responseMessage) {
        long time =
            Math.abs(Duration.between(responseMessage.getTimeCreated(), OffsetDateTime.now())
                .toMillis());
        responseMessage
            .editMessageFormat("Changed: %d ms", time)
            .queue();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if (pacilmateUtility.isQuery(message.getContentRaw(), "ping")) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage("Pong!")
                .queue(this::pongAccepted);
        }
    }

}
