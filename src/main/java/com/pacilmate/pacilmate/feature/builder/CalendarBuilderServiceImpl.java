package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalendarBuilderServiceImpl implements CalendarBuilderService {

    @Autowired
    PacilmateUserRepository pacilmateUserRepository;

    @Autowired
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Override
    public PacilmateCalendar makeCalendar(long userId, String title) {
        PacilmateUser pacilmateUser = pacilmateUserRepository.findByUserId(userId);
        PacilmateCalendar pacilmateCalendar =
            new PacilmateCalendar(title, pacilmateUser, pacilmateUtility.randomColor());
        return pacilmateCalendarRepository.save(pacilmateCalendar);
    }
}
