package com.pacilmate.pacilmate.feature.builder;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface BuilderMediator {
    Message buildCalendar(User author, String command);

    Message buildEvent(User author, String command);
}
