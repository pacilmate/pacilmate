package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BuilderListener extends ListenerAdapter {

    @Autowired
    private BuilderMediator builderMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        MessageChannel channel = event.getChannel();
        if (pacilmateUtility.isQuery(rawContent, "makecal")) {
            Message returnMessage = builderMediator.buildCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "makeev")) {
            Message returnMessage = builderMediator.buildEvent(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
