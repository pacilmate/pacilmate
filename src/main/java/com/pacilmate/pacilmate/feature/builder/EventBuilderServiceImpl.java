package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import java.time.OffsetDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventBuilderServiceImpl implements EventBuilderService {

    @Autowired
    PacilmateUserRepository pacilmateUserRepository;

    @Autowired
    PacilmateEventRepository pacilmateEventRepository;

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Override
    public PacilmateEvent makeEvent(long userId, String description, OffsetDateTime time) {
        PacilmateUser pacilmateUser = pacilmateUserRepository.findByUserId(userId);
        PacilmateEvent pacilmateEvent =
            new PacilmateEvent(description, time, pacilmateUser, pacilmateUtility.randomColor());
        return pacilmateEventRepository.save(pacilmateEvent);
    }
}
