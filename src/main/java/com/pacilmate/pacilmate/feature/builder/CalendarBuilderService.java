package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import org.springframework.stereotype.Service;

@Service
public interface CalendarBuilderService {
    PacilmateCalendar makeCalendar(long userId, String title);
}
