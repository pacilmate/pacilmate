package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import java.time.OffsetDateTime;
import org.springframework.stereotype.Service;

@Service
public interface EventBuilderService {
    PacilmateEvent makeEvent(long userId, String description, OffsetDateTime time);
}
