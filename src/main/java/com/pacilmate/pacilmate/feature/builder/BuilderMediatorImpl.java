package com.pacilmate.pacilmate.feature.builder;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEmbedBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import io.micrometer.core.annotation.Timed;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuilderMediatorImpl implements BuilderMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    CalendarBuilderService calendarBuilderService;

    @Autowired
    EventBuilderService eventBuilderService;

    @Autowired
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @Autowired
    PacilmateEmbedBuilder<PacilmateCalendar> pacilmateCalendarEmbedBuilder;

    @Autowired
    PacilmateEmbedBuilder<PacilmateEvent> pacilmateEventEmbedBuilder;

    @Timed("build_calendar")
    @Override
    public Message buildCalendar(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 1);
            PacilmateCalendar pacilmateCalendar =
                calendarBuilderService.makeCalendar(author.getIdLong(), arguments.get(0));
            messageBuilder.setContent(
                String.format("Calendar **%s** has been successfully made!", arguments.get(0)));
            messageBuilder.setEmbed(pacilmateCalendarEmbedBuilder.makeEmbed(pacilmateCalendar));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("build_event")
    @Override
    public Message buildEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 3);
            OffsetDateTime dateTime =
                pacilmateDateTimeParser.parse(arguments.get(1), arguments.get(2));
            PacilmateEvent pacilmateEvent =
                eventBuilderService.makeEvent(author.getIdLong(), arguments.get(0), dateTime);
            messageBuilder.setContent(
                String.format("The event **%s** has been successfully made!", arguments.get(0)));
            messageBuilder.setEmbed(pacilmateEventEmbedBuilder.makeEmbed(pacilmateEvent));
        } catch (PacilmateUnverifiedCommandException | PacilmateParseDateTimeException
            | DateTimeParseException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();

    }
}
