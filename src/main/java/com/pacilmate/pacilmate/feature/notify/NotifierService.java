package com.pacilmate.pacilmate.feature.notify;

import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface NotifierService {
    void scheduledNotifier();

    void createTimer(Long minutes, User author);
}
