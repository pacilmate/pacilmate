package com.pacilmate.pacilmate.feature.notify;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface NotifierMediator {
    Message createTimer(User author, String rawMessage);
}
