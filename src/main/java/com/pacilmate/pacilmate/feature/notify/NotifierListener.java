package com.pacilmate.pacilmate.feature.notify;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class NotifierListener extends ListenerAdapter {

    @Autowired
    private NotifierMediator notifierMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        MessageChannel channel = event.getChannel();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        if (pacilmateUtility.isQuery(rawContent, "remindme")) {
            Message returnMessage = notifierMediator.createTimer(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
