package com.pacilmate.pacilmate.feature.notify;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import io.micrometer.core.annotation.Timed;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NotifierMediatorImpl implements NotifierMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    NotifierService notifierService;

    @Timed("create_timer")
    @Override
    public Message createTimer(User author, String rawMessage) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> args = pacilmateUtility.verify(author, rawMessage, 1);
            Long duration = Long.parseLong(args.get(0));
            notifierService.createTimer(duration, author);
            messageBuilder.setContent(
                String.format("I'll remind you in %d %s",
                    duration,
                    duration == 1 ? "minute" : "minutes"));
            return messageBuilder.build();
        } catch (Exception e) {
            messageBuilder.setContent(
                "Sorry, there was a problem running the command " + author.getAsMention()
            );
        }
        return messageBuilder.build();
    }
}
