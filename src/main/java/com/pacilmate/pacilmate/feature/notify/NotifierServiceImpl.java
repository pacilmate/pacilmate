package com.pacilmate.pacilmate.feature.notify;

import com.pacilmate.pacilmate.jda.PacilmateJdaService;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import java.awt.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
public class NotifierServiceImpl implements NotifierService {

    @Autowired
    PacilmateCalendarRepository calendarRepository;

    @Autowired
    PacilmateEventRepository eventRepository;

    @Autowired
    PacilmateUserRepository userRepository;

    @Autowired
    private PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @Autowired
    private PacilmateJdaService jdaService;


    @Scheduled(cron = "0 0 */1 * * *")
    @Override
    public void scheduledNotifier() {
        checkForUpcomingEvent();
    }

    void checkForUpcomingEvent() {
        OffsetDateTime now = OffsetDateTime.now();
        List<PacilmateCalendar> calendars = calendarRepository.findAll();

        for (PacilmateCalendar calendar : calendars) {
            List<PacilmateEvent> events = calendar.getEventList();
            List<PacilmateEvent> eventsAboutToHappen = new ArrayList<>();
            List<PacilmateUser> subscribers = calendar.getSubscriber();

            for (PacilmateEvent event : events) {
                if (event.getTime().isAfter(now)
                    && event.getTime().isBefore(now.plusHours(2))) {
                    eventsAboutToHappen.add(event);
                }
            }

            if (!eventsAboutToHappen.isEmpty()) {
                sendReminderDM(subscribers, eventsAboutToHappen);
            }
        }
    }

    void sendReminderDM(List<PacilmateUser> users, List<PacilmateEvent> events) {
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.setContent(
            "A friendly reminder that you will have these events in the next 2 hours:"
        );

        StringBuilder msg = new StringBuilder();
        for (PacilmateEvent event : events) {
            msg.append(pacilmateEventMessageBuilder.makeMessage(event));
            msg.append("\n");
        }

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setDescription(msg);
        embedBuilder.setColor(new Color(255, 182, 64));

        messageBuilder.setEmbed(embedBuilder.build());

        for (PacilmateUser user : users) {
            JDA jda = jdaService.getjda();
            jda.openPrivateChannelById(user.getUserId()).queue(
                privateChannel -> privateChannel.sendMessage(messageBuilder.build()).queue()
            );
        }
    }

    @Override
    public void createTimer(Long minutes, User author) {
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.setContent(
            String.format("Timer stops! You asked me to remind you %d %s ago.",
                minutes,
                minutes == 1 ? "minute" : "minutes"));

        ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
        timer.schedule(() -> author.openPrivateChannel().queue(
            privateChannel -> privateChannel.sendMessage(messageBuilder.build()).queue()
        ), minutes, TimeUnit.MINUTES);
    }
}
