package com.pacilmate.pacilmate.feature.manage.calendar;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


@Controller
public class ManageCalendarListener extends ListenerAdapter {

    @Autowired
    private ManageCalendarMediator manageCalendarMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        MessageChannel channel = event.getChannel();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        if (pacilmateUtility.isQuery(rawContent, "sub")) {
            Message returnMessage = manageCalendarMediator.subCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();

        } else if (pacilmateUtility.isQuery(rawContent, "unsub")) {
            Message returnMessage = manageCalendarMediator.unsubCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
