package com.pacilmate.pacilmate.feature.manage.event;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManageEventServiceImpl implements ManageEventService {

    @Autowired
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Autowired
    PacilmateEventRepository pacilmateEventRepository;

    @Autowired
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;

    @Override
    public MessageBuilder setEvent(long userId, int calendarId, int eventId) {
        MessageBuilder messageBuilder = new MessageBuilder();
        PacilmateCalendar pacilmateCalendar = pacilmateCalendarRepository.findByCalendarId(
            calendarId);
        PacilmateEvent pacilmateEvent = pacilmateEventRepository.findByEventId(eventId);
        if (pacilmateCalendar == null) {
            messageBuilder.setContent("Whoops! Sorry, this calendar does not exist!");
        } else if (pacilmateEvent == null) {
            messageBuilder.setContent("Whoops! Sorry, this event does not exist!");
        } else if (!pacilmateCalendar.getAuthor().getUserId().equals(userId)) {
            messageBuilder.setContent("Whoops! Sorry, this calendar does not belong to you.");
        } else {
            pacilmateCalendar.getEventList().add(pacilmateEvent);
            pacilmateCalendarRepository.save(pacilmateCalendar);
            pacilmateEvent.getCalendarList().add(pacilmateCalendar);
            pacilmateEventRepository.save(pacilmateEvent);
            messageBuilder.setContent(String.format("Successfully set **%s** to **%s**",
                pacilmateEvent.getDescription(), pacilmateCalendar.getDescription()));
            messageBuilder.setEmbed(pacilmateManageEventEmbedBuilder.makeEmbed(pacilmateCalendar));
        }
        return messageBuilder;
    }

    @Override
    public MessageBuilder unsetEvent(long userId, int calendarId, int eventId) {
        MessageBuilder messageBuilder = new MessageBuilder();
        PacilmateCalendar pacilmateCalendar = pacilmateCalendarRepository.findByCalendarId(
            calendarId);
        PacilmateEvent pacilmateEvent = pacilmateEventRepository.findByEventId(eventId);
        if (pacilmateCalendar == null) {
            messageBuilder.setContent("Whoops! Sorry, this calendar does not exist!");
        } else if (pacilmateEvent == null) {
            messageBuilder.setContent("Whoops! Sorry, this event does not exist!");
        } else if (!pacilmateCalendar.getAuthor().getUserId().equals(userId)) {
            messageBuilder.setContent("Whoops! Sorry, this calendar does not belong to you.");
        } else {
            List<PacilmateEvent> eventList = pacilmateCalendar.getEventList();
            List<PacilmateCalendar> calendarList = pacilmateEvent.getCalendarList();
            for (PacilmateEvent event : eventList) {
                if (event.getEventId().equals(pacilmateEvent.getEventId())) {
                    eventList.remove(event);
                    break;
                }
            }
            for (PacilmateCalendar calendar : calendarList) {
                if (calendar.getCalendarId().equals(pacilmateCalendar.getCalendarId())) {
                    calendarList.remove(calendar);
                    break;
                }
            }
            pacilmateCalendarRepository.save(pacilmateCalendar);
            pacilmateEventRepository.save(pacilmateEvent);
            messageBuilder.setContent(String.format("Successfully unset **%s** from **%s**",
                pacilmateEvent.getDescription(), pacilmateCalendar.getDescription()));
            messageBuilder.setEmbed(pacilmateManageEventEmbedBuilder.makeEmbed(pacilmateCalendar));
        }
        return messageBuilder;
    }
}
