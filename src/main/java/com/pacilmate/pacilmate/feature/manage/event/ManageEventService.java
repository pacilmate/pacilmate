package com.pacilmate.pacilmate.feature.manage.event;

import net.dv8tion.jda.api.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public interface ManageEventService {
    MessageBuilder setEvent(long userId, int calendarId, int eventId);

    MessageBuilder unsetEvent(long userId, int calendarId, int eventId);
}
