package com.pacilmate.pacilmate.feature.manage.calendar;

import com.pacilmate.pacilmate.exception.PacilmateUserAlreadySubscribedException;
import com.pacilmate.pacilmate.exception.PacilmateUserNotSubscribedException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;

public interface ManageCalendarService {
    PacilmateCalendar subCalendar(long userId, int calendarId)
        throws PacilmateUserAlreadySubscribedException;

    PacilmateCalendar unsubCalendar(long userId, int calendarId)
        throws PacilmateUserNotSubscribedException;
}
