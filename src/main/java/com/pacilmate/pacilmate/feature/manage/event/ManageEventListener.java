package com.pacilmate.pacilmate.feature.manage.event;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ManageEventListener extends ListenerAdapter {

    @Autowired
    private ManageEventMediator manageEventMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        MessageChannel channel = event.getChannel();
        if (pacilmateUtility.isQuery(rawContent, "set")) {
            Message returnMessage = manageEventMediator.setEvent(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "unset")) {
            Message returnMessage = manageEventMediator.unsetEvent(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
