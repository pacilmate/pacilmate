package com.pacilmate.pacilmate.feature.manage.calendar;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public interface ManageCalendarMediator {
    Message subCalendar(User author, String rawMessage);

    Message unsubCalendar(User author, String rawMessage);
}
