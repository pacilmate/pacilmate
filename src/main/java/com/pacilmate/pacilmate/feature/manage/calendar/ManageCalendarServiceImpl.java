package com.pacilmate.pacilmate.feature.manage.calendar;

import com.pacilmate.pacilmate.exception.PacilmateUserAlreadySubscribedException;
import com.pacilmate.pacilmate.exception.PacilmateUserNotSubscribedException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ManageCalendarServiceImpl implements ManageCalendarService {

    @Autowired
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Autowired
    PacilmateUserRepository pacilmateUserRepository;

    @Override
    public PacilmateCalendar subCalendar(long userId, int calendarId)
        throws PacilmateUserAlreadySubscribedException {
        PacilmateUser pacilmateUser = pacilmateUserRepository.findByUserId(userId);
        PacilmateCalendar pacilmateCalendar
            = pacilmateCalendarRepository.findByCalendarId(calendarId);
        List<PacilmateUser> subscribers = pacilmateCalendar.getSubscriber();

        for (PacilmateUser subscriber : subscribers) {
            if (subscriber.getUserId().equals(pacilmateUser.getUserId())) {
                throw new PacilmateUserAlreadySubscribedException(
                    "You are already subscribed to this calendar");
            }
        }

        subscribers.add(pacilmateUser);
        pacilmateCalendarRepository.save(pacilmateCalendar);
        return pacilmateCalendar;
    }

    @Override
    public PacilmateCalendar unsubCalendar(long userId, int calendarId)
        throws PacilmateUserNotSubscribedException {
        PacilmateUser pacilmateUser = pacilmateUserRepository.findByUserId(userId);
        PacilmateCalendar pacilmateCalendar
            = pacilmateCalendarRepository.findByCalendarId(calendarId);
        List<PacilmateUser> subscribers = pacilmateCalendar.getSubscriber();

        boolean exist = false;
        for (PacilmateUser subscriber : subscribers) {
            if (subscriber.getUserId().equals(pacilmateUser.getUserId())) {
                subscribers.remove(subscriber);
                exist = true;
                break;
            }
        }
        if (!exist) {
            throw new PacilmateUserNotSubscribedException(
                "You are not subscribed to this calendar yet");
        }
        pacilmateCalendarRepository.save(pacilmateCalendar);
        return pacilmateCalendar;
    }


}
