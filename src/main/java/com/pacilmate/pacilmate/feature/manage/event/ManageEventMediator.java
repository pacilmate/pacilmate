package com.pacilmate.pacilmate.feature.manage.event;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface ManageEventMediator {
    Message setEvent(User author, String command);

    Message unsetEvent(User author, String command);
}
