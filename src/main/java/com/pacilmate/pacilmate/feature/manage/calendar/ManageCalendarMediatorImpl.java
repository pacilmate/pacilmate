package com.pacilmate.pacilmate.feature.manage.calendar;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.exception.PacilmateUserAlreadySubscribedException;
import com.pacilmate.pacilmate.exception.PacilmateUserNotSubscribedException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import io.micrometer.core.annotation.Timed;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ManageCalendarMediatorImpl implements ManageCalendarMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    ManageCalendarService manageCalendarService;

    @Timed("sub_calendar")
    @Override
    public Message subCalendar(User author, String rawMessage) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            List<String> arguments
                = pacilmateUtility.verify(author, rawMessage, 1);
            PacilmateCalendar pacilmateCalendar
                = manageCalendarService.subCalendar(
                author.getIdLong(), Integer.parseInt(arguments.get(0)));
            messageBuilder.setContent(
                String.format("Successfully **subscribed** to **%s**! %s",
                    pacilmateCalendar.getDescription(),
                    author.getAsMention()));

        } catch (PacilmateUserAlreadySubscribedException
            | PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());

        } catch (Exception e) {
            messageBuilder.setContent(
                String.format(
                    "Sorry, there was a problem running the command %s",
                    author.getAsMention()));
        }


        return messageBuilder.build();
    }

    @Timed("unsub_calendar")
    @Override
    public Message unsubCalendar(User author, String rawMessage) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            List<String> arguments = pacilmateUtility.verify(author, rawMessage, 1);
            PacilmateCalendar pacilmateCalendar
                = manageCalendarService.unsubCalendar(
                author.getIdLong(),
                Integer.parseInt(arguments.get(0)));
            messageBuilder.setContent(
                String.format(
                    "Successfully **unsubscribed** to **%s**! %s",
                    pacilmateCalendar.getDescription(),
                    author.getAsMention()));

        } catch (PacilmateUserNotSubscribedException | PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());

        } catch (Exception e) {
            messageBuilder.setContent(
                String.format(
                    "Sorry, there was a problem running the command %s",
                    author.getAsMention()));
        }

        return messageBuilder.build();
    }
}
