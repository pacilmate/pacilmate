package com.pacilmate.pacilmate.feature.manage.event;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import io.micrometer.core.annotation.Timed;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManageEventMediatorImpl implements ManageEventMediator {

    @Autowired
    ManageEventService manageEventService;

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Timed("set_event")
    @Override
    public Message setEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 2);
            int calenderId = Integer.parseInt(arguments.get(0));
            int eventId = Integer.parseInt(arguments.get(1));
            messageBuilder = manageEventService.setEvent(author.getIdLong(), calenderId, eventId);
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }

    @Timed("unset_event")
    @Override
    public Message unsetEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 2);
            int calendarId = Integer.parseInt(arguments.get(0));
            int eventId = Integer.parseInt(arguments.get(1));
            messageBuilder = manageEventService.unsetEvent(author.getIdLong(), calendarId, eventId);
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }
}
