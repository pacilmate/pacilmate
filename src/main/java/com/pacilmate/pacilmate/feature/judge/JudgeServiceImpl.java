package com.pacilmate.pacilmate.feature.judge;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@Slf4j
public class JudgeServiceImpl implements JudgeService {

    private static final String PROBLEM_URL = "/view";
    private static final String SCOREBOARD_URL = "/scoreboard";
    private static final String SOLVE_URL = "/solve";
    private static final String USER_URL = "/user";
    private static final String INIT_URL = "/init";
    private static final String ANSWER_URL = "/answer";
    private static final String RELEASE_URL = "/release";

    private static final String USER_ID = "userId";
    private static final String PROBLEM_ID = "problemId";
    private static final String USERNAME = "username";
    private static final String STATUS = "status";
    private static final String ANSWER = "answer";

    private final WebClient.Builder webClientBuilder;

    @Value("${paciljudge.datasource.url}")
    private String paciljudgeUrl;

    @Autowired
    public JudgeServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public JsonNode getProblems() {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();
        return webClient.get()
            .uri(PROBLEM_URL).retrieve()
            .bodyToMono(JsonNode.class).block();
    }

    @Override
    public JsonNode solveProblem(long userId, String problemId) {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode postBody = mapper.createObjectNode();

        postBody.put(USER_ID, userId);
        postBody.put(PROBLEM_ID, problemId);

        return webClient.post().uri(SOLVE_URL)
            .bodyValue(postBody).retrieve()
            .bodyToMono(JsonNode.class).block();
    }

    @Override
    public Integer releaseProblem(long userId) {

        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode postBody = mapper.createObjectNode();

        postBody.put(USER_ID, userId);

        return Objects.requireNonNull(webClient.post().uri(RELEASE_URL)
            .bodyValue(postBody).retrieve()
            .bodyToMono(JsonNode.class).block())
            .get(STATUS).asInt();
    }

    @Override
    public JsonNode answerProblem(long userId, String answer) {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode postBody = mapper.createObjectNode();

        postBody.put(USER_ID, userId);
        postBody.put(ANSWER, answer);

        return webClient.post().uri(ANSWER_URL)
            .bodyValue(postBody).retrieve()
            .bodyToMono(JsonNode.class).block();
    }

    @Override
    public JsonNode checkUser(long userId) {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();

        return webClient.get().uri(USER_URL + "?id=" + userId)
            .retrieve().bodyToMono(JsonNode.class).block();
    }

    @Override
    public JsonNode scoreboard() {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();
        return webClient.get()
            .uri(SCOREBOARD_URL).retrieve()
            .bodyToMono(JsonNode.class).block();
    }

    @Override
    public JsonNode initUser(long userId, String username) {
        WebClient webClient = webClientBuilder.baseUrl(paciljudgeUrl).build();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode postBody = mapper.createObjectNode();

        postBody.put(USER_ID, userId);
        postBody.put(USERNAME, username);

        return webClient.post().uri(INIT_URL)
            .bodyValue(postBody).retrieve()
            .bodyToMono(JsonNode.class).block();
    }
}
