package com.pacilmate.pacilmate.feature.judge;

import com.fasterxml.jackson.databind.JsonNode;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEmbedBuilder;
import io.micrometer.core.annotation.Timed;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientRequestException;

@Service
public class JudgeMediatorImpl implements JudgeMediator {

    private static final String STATUS = "status";
    private static final String NO_INIT_MSG =
        "Please do init first before using **PacilJudge!** 😊";
    private static final String MESSAGE = "message";

    @Autowired
    PacilmateUtility pacilmateUtility;
    @Autowired
    JudgeService judgeService;
    @Autowired
    PacilmateEmbedBuilder<JsonNode> paciljudgeUserEmbedBuilder;
    @Autowired
    PacilmateEmbedBuilder<JsonNode> paciljudgeScoreboardEmbedBuilder;
    @Autowired
    PacilmateEmbedBuilder<JsonNode> paciljudgeSessionEmbedBuilder;
    @Autowired
    PacilmateEmbedBuilder<JsonNode> paciljudgeProblemsEmbedBuilder;
    @Autowired
    PacilmateEmbedBuilder<JsonNode> paciljudgeVerdictEmbedBuilder;

    @Timed("get_problem")
    @Override
    public Message getAllProblems() {
        JsonNode response = judgeService.getProblems();
        return (new MessageBuilder()).setEmbed(paciljudgeProblemsEmbedBuilder.makeEmbed(response))
            .build();
    }

    @Timed("solve_problem")
    @Override
    public Message solveProblem(User user, String rawContent) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            List<String> arguments = pacilmateUtility.verify(user, rawContent, 1);
            JsonNode response = judgeService.solveProblem(user.getIdLong(), arguments.get(0));
            int status = response.get(STATUS).asInt();

            if (status == 200) {
                messageBuilder.setEmbed(paciljudgeSessionEmbedBuilder.makeEmbed(response));
            } else {
                messageBuilder.setContent(response.get(MESSAGE).asText() + " 😭");
            }

        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("release_problem")
    @Override
    public Message releaseProblem(User user) {
        Integer status = judgeService.releaseProblem(user.getIdLong());
        MessageBuilder messageBuilder = new MessageBuilder();
        if (status == 200) {
            messageBuilder.setContent(
                "📚 Your currently running problem has been released! " + user.getAsMention()
            );
        } else if (status == 204) {
            messageBuilder.setContent(
                "📉 You don't have any running problem! " + user.getAsMention()
            );
        } else {
            messageBuilder.setContent(
                NO_INIT_MSG
            );
        }
        return messageBuilder.build();
    }

    @Timed("answer_problem")
    @Override
    public Message answerProblem(User user, String rawContent) {

        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(user, rawContent, 1);
            JsonNode response = judgeService.answerProblem(user.getIdLong(), arguments.get(0));

            int status = response.get(STATUS).asInt();

            if (status == 200) {
                messageBuilder.setEmbed(paciljudgeVerdictEmbedBuilder.makeEmbed(response));
            } else {
                messageBuilder.setContent("😭 No currently active sessions");
            }
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }

    @Timed("check_profile")
    @Override
    public Message checkProfile(User user) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            JsonNode response = judgeService.checkUser(user.getIdLong());
            int status = response.get(STATUS).asInt();

            if (status == 200) {
                messageBuilder.setEmbed(paciljudgeUserEmbedBuilder.makeEmbed(response));
            } else {
                messageBuilder.setContent(NO_INIT_MSG);
            }
        } catch (WebClientRequestException e) {
            messageBuilder.setContent("PacilJudge is Offline! 🤓");
        }

        return messageBuilder.build();
    }

    @Timed("scoreboard")
    @Override
    public Message scoreboard() {
        JsonNode response = judgeService.scoreboard();
        return (new MessageBuilder()).setEmbed(paciljudgeScoreboardEmbedBuilder.makeEmbed(response))
            .build();
    }

    @Timed("init_user")
    @Override
    public Message initUser(User user) {
        JsonNode response = judgeService.initUser(user.getIdLong(), user.getName());
        return (new MessageBuilder()).setEmbed(paciljudgeUserEmbedBuilder.makeEmbed(response))
            .build();
    }
}
