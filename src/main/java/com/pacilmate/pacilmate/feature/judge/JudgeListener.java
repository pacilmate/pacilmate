package com.pacilmate.pacilmate.feature.judge;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class JudgeListener extends ListenerAdapter {

    private static final String LOADING =
        "<:Loading_1:846098059850678312> <a:Loading_2:846098120549859378>";

    private static final String LOADING_EMOJI = "⌛";

    @Autowired
    private JudgeMediator judgeMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    public void profileAccepted(Message responseMessage, User author) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.checkProfile(author);
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void initAccepted(Message responseMessage, User author) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.initUser(author);
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void problemAccepted(Message responseMessage) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.getAllProblems();
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void solveAccepted(Message responseMessage, User author, String rawContent) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.solveProblem(author, rawContent);
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void releaseAccepted(Message responseMessage, User author) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.releaseProblem(author);
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void answerAccepted(Message responseMessage, User author, String rawContent) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.answerProblem(author, rawContent);
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    public void scoreboardAccepted(Message responseMessage) {
        responseMessage.addReaction(LOADING_EMOJI).queue();
        Message returnMessage = judgeMediator.scoreboard();
        responseMessage.editMessage(returnMessage).override(true).queue();
        responseMessage.clearReactions().queue();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();
        MessageChannel channel = event.getChannel();
        if (pacilmateUtility.isQuery(rawContent, "init")) {
            channel.sendMessage(LOADING).queue(sentMessage -> initAccepted(sentMessage, author));
        } else if (pacilmateUtility.isQuery(rawContent, "problems")) {
            channel.sendMessage(LOADING).queue(this::problemAccepted);
        } else if (pacilmateUtility.isQuery(rawContent, "solve")) {
            channel.sendMessage(LOADING)
                .queue(sentMessage -> solveAccepted(sentMessage, author, rawContent));
        } else if (pacilmateUtility.isQuery(rawContent, "release")) {
            channel.sendMessage(LOADING).queue(sentMessage -> releaseAccepted(sentMessage, author));
        } else if (pacilmateUtility.isQuery(rawContent, "answer")) {
            channel.sendMessage(LOADING)
                .queue(sentMessage -> answerAccepted(sentMessage, author, rawContent));
        } else if (pacilmateUtility.isQuery(rawContent, "profile")) {
            channel.sendMessage(LOADING).queue(sentMessage -> profileAccepted(sentMessage, author));
        } else if (pacilmateUtility.isQuery(rawContent, "scoreboard")) {
            channel.sendMessage(LOADING).queue(this::scoreboardAccepted);
        }
    }
}
