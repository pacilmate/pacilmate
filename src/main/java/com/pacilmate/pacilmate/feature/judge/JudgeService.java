package com.pacilmate.pacilmate.feature.judge;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;

@Service
public interface JudgeService {
    JsonNode getProblems();

    JsonNode solveProblem(long userId, String problemId);

    Integer releaseProblem(long userId);

    JsonNode answerProblem(long userId, String answer);

    JsonNode checkUser(long userId);

    JsonNode scoreboard();

    JsonNode initUser(long userId, String username);
}
