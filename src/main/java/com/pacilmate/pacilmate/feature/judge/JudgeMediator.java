package com.pacilmate.pacilmate.feature.judge;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface JudgeMediator {
    Message getAllProblems();

    Message solveProblem(User user, String rawContent);

    Message releaseProblem(User user);

    Message answerProblem(User user, String rawContent);

    Message checkProfile(User user);

    Message scoreboard();

    Message initUser(User user);
}
