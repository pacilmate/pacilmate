package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LookupCalendarServiceImpl implements LookupCalendarService {

    @Autowired
    private PacilmateUserRepository userRepository;

    @Override
    public List<PacilmateCalendar> getListSubscribedCalendar(Long userId) {

        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);

        return pacilmateUser.getSubscribedCalendar();
    }

    @Override
    public List<PacilmateCalendar> getListMyCalendar(Long userId) {

        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);

        return pacilmateUser.getMadeCalendar();
    }
}
