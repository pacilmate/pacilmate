package com.pacilmate.pacilmate.feature.lookup.specific;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class LookupSpecificListener extends ListenerAdapter {

    @Autowired
    private LookupSpecificMediator lookupSpecificMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        MessageChannel channel = event.getChannel();
        if (pacilmateUtility.isQuery(rawContent, "cal")) {
            Message returnMessage = lookupSpecificMediator.lookupCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
