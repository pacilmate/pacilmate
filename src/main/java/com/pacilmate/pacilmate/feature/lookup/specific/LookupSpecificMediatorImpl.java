package com.pacilmate.pacilmate.feature.lookup.specific;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LookupSpecificMediatorImpl implements LookupSpecificMediator {

    @Autowired
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Autowired
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Override
    public Message lookupCalendar(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 1);
            int calendarId = Integer.parseInt(arguments.get(0));
            PacilmateCalendar pacilmateCalendar = pacilmateCalendarRepository
                    .findByCalendarId(calendarId);
            messageBuilder.setEmbed(pacilmateManageEventEmbedBuilder.makeEmbed(pacilmateCalendar));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }
}
