package com.pacilmate.pacilmate.feature.lookup.query;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CalendarLookupServiceImpl implements CalendarLookupService {

    @Autowired
    PacilmateEventMessageBuilder pacilmateEventMessageBuilder;
    @Autowired
    private PacilmateUserRepository userRepository;


    @Override
    public List<PacilmateEvent> getFirstEvent(OffsetDateTime date, Long userId, String number) {
        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);
        List<PacilmateEvent> events = pacilmateUser.getMadeEvent();
        List<PacilmateEvent> listevent = new ArrayList<>();
        List<PacilmateEvent> targetevent = new ArrayList<>();
        int targetDate = date.getDayOfYear();
        int targetYear = date.getYear();
        int num = Integer.parseInt(number);
        for (int i = 0; i < events.size(); i++) {
            if ((targetDate <= events.get(i).getTime().getDayOfYear())
                && (targetYear <= events.get(i).getTime().getYear())) {
                listevent.add(events.get(i));
            }
        }


        for (int j = 0; j < num; j++) {
            targetevent.add(listevent.get(j));
        }


        return targetevent;
    }

    @Override
    public List<PacilmateEvent> getListSpecifiedEvent(Long userId, String number) {
        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);
        List<PacilmateEvent> events = pacilmateUser.getMadeEvent();
        List<PacilmateEvent> listevent = new ArrayList<>();
        for (PacilmateEvent ev : events) {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
            String formattedDate = formatter.format(ev.getTime());

            String[] dateParts = formattedDate.split("-");
            String day = dateParts[2];
            String month = dateParts[1];
            String year = dateParts[0];
            String years = day + "-" + month + "-" + year;

            String months = day + "-" + month;

            if (years.equals(number) || months.equals(number) || day.equals(number)) {
                listevent.add(ev);
            }
        }
        return listevent;
    }

}


