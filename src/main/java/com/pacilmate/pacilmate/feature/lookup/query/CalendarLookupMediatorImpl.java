package com.pacilmate.pacilmate.feature.lookup.query;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import io.micrometer.core.annotation.Timed;
import java.awt.*;
import java.time.OffsetDateTime;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CalendarLookupMediatorImpl implements CalendarLookupMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    PacilmateUserRepository pacilmateUserRepository;

    @Autowired
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @Autowired
    CalendarLookupService calendarLookupService;

    @Autowired
    private PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @Timed("list_first_event")
    @Override
    public Message listFirstEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();

        EmbedBuilder embedBuilder = new EmbedBuilder();
        OffsetDateTime date = OffsetDateTime.now();


        try {
            List<String> arguments =
                pacilmateUtility.verify(author, command, 1);
            messageBuilder
                .setContent(("** Here's your " + arguments.get(0) + " next event(s)**\n"));

            List<PacilmateEvent> events =
                calendarLookupService.getFirstEvent(date, author.getIdLong(), arguments.get(0));

            StringBuilder message = new StringBuilder();
            for (PacilmateEvent ev : events) {
                message.append(pacilmateEventMessageBuilder.makeMessage(ev)).append("\n");
            }
            embedBuilder.setDescription(message);
            embedBuilder.setAuthor(author.getName());
            embedBuilder.setColor(Color.blue);
            messageBuilder.setEmbed(embedBuilder.build());
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        } catch (Exception e) {
            messageBuilder.setContent(
                String.format(
                    "You don't have that much events %s",
                    author.getAsMention()));
        }

        return messageBuilder.build();
    }

    @Timed("list_specified_event")
    @Override
    public Message listSpecifiedEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();

        EmbedBuilder embedBuilder = new EmbedBuilder();


        try {
            List<String> arguments =
                pacilmateUtility.verify(author, command, 1);


            List<PacilmateEvent> events =
                calendarLookupService.getListSpecifiedEvent(author.getIdLong(), arguments.get(0));
            if (events.isEmpty()) {
                messageBuilder.setContent(
                    String.format("You don't have any events for %s.", arguments.get(0))
                );
            } else {
                messageBuilder
                    .setContent(("** Here's your event for " + arguments.get(0) + "**\n"));
                StringBuilder message = new StringBuilder();
                for (PacilmateEvent ev : events) {
                    message.append(pacilmateEventMessageBuilder.makeMessage(ev)).append("\n");
                }

                embedBuilder.setDescription(message);
                embedBuilder.setAuthor(author.getName());
                embedBuilder.setColor(Color.green);
                messageBuilder.setEmbed(embedBuilder.build());
            }


        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

}





