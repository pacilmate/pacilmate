package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalEventEmbedBuilder;
import io.micrometer.core.annotation.Timed;
import java.awt.*;
import java.time.OffsetDateTime;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LookupGlobalMediatorImpl implements LookupGlobalMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    LookupCalendarService lookupCalendarService;

    @Autowired
    LookupEventService lookupEventService;

    @Autowired
    PacilmateGlobalCalendarEmbedBuilder globalCalendarEmbedBuilder;

    @Autowired
    PacilmateGlobalEventEmbedBuilder globalEventEmbedBuilder;

    @Timed("get_calendar_list")
    @Override
    public Message getListCalendar(String command, User author) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            pacilmateUtility.verify(author, command, 0);

            List<PacilmateCalendar> calendars =
                lookupCalendarService.getListSubscribedCalendar(author.getIdLong());

            if (calendars.isEmpty()) {
                messageBuilder.setContent("You haven't subscribed to any calendars.");
            } else {
                messageBuilder.setContent("** 📌 Here's list all of your subscribed calendar(s).**");
            }

            globalCalendarEmbedBuilder.setAuthor(new PacilmateUser(author));
            messageBuilder.setEmbed(globalCalendarEmbedBuilder.makeEmbed(calendars));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("get_my_calendar_list")
    @Override
    public Message getListMyCalendar(String command, User author) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            pacilmateUtility.verify(author, command, 0);
            List<PacilmateCalendar> calendars =
                lookupCalendarService.getListMyCalendar(author.getIdLong());

            if (calendars.isEmpty()) {
                messageBuilder.setContent("You haven't made any calendars.");
            } else {
                messageBuilder.setContent(("** 📌 Here's list all calendar(s) made by you.**"));
            }

            globalCalendarEmbedBuilder.setAuthor(new PacilmateUser(author));
            messageBuilder.setEmbed(globalCalendarEmbedBuilder.makeEmbed(calendars));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("get_my_event_list")
    @Override
    public Message getListMyEvent(String command, User author) {
        MessageBuilder messageBuilder = new MessageBuilder();

        try {
            pacilmateUtility.verify(author, command, 0);

            List<PacilmateEvent> events = lookupEventService.getMyEvent(author.getIdLong());
            if (events.isEmpty()) {
                messageBuilder.setContent("You haven't made any events.");
            } else {
                messageBuilder.setContent(("** 📌 Here's list all event(s) made by you.**"));
            }

            globalEventEmbedBuilder.setAuthor(new PacilmateUser(author));
            messageBuilder.setEmbed(globalEventEmbedBuilder.makeEmbed(events));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("get_event_list")
    @Override
    public Message getListEvent(String command, User author) {
        MessageBuilder messageBuilder = new MessageBuilder();

        OffsetDateTime date = OffsetDateTime.now();
        if (command.equals("tomorrow")) {
            date = date.plusDays(1);
        }

        try {
            pacilmateUtility.verify(author, command, 0);

            List<PacilmateEvent> events = lookupEventService.getSpecifiedEvent(
                date, author.getIdLong()
            );

            if (events.isEmpty()) {
                messageBuilder.setContent(
                        String.format("You don't have any events for %s.", command)
                );
            } else {
                messageBuilder.setContent(
                        String.format("** 📌 Here's list all event(s) for %s.**", command)
                );
            }

            globalEventEmbedBuilder.setAuthor(new PacilmateUser(author));
            messageBuilder.setEmbed(globalEventEmbedBuilder.makeEmbed(events));
        } catch (PacilmateUnverifiedCommandException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }
}
