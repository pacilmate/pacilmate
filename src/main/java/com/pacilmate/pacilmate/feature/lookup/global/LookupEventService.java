package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface LookupEventService {
    List<PacilmateEvent> getMyEvent(long userId);

    List<PacilmateEvent> getSpecifiedEvent(OffsetDateTime dateTime, Long userId);
}
