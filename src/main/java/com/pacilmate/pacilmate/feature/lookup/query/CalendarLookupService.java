package com.pacilmate.pacilmate.feature.lookup.query;


import com.pacilmate.pacilmate.model.PacilmateEvent;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface CalendarLookupService {
    List<PacilmateEvent> getFirstEvent(OffsetDateTime date, Long userId, String number);

    List<PacilmateEvent> getListSpecifiedEvent(Long userId, String number);
}


