package com.pacilmate.pacilmate.feature.lookup.global;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface LookupGlobalMediator {
    Message getListCalendar(String command, User author);

    Message getListMyCalendar(String command, User author);

    Message getListMyEvent(String command, User author);

    Message getListEvent(String command, User author);
}
