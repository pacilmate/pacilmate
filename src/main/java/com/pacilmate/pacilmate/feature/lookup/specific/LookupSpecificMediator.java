package com.pacilmate.pacilmate.feature.lookup.specific;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public interface LookupSpecificMediator {
    Message lookupCalendar(User author, String command);
}
