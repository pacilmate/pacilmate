package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LookupEventServiceImpl implements LookupEventService {

    @Autowired
    private PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @Autowired
    private PacilmateUserRepository userRepository;

    @Override
    public List<PacilmateEvent> getMyEvent(long userId) {

        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);

        return pacilmateUser.getMadeEvent();
    }

    @Override
    public List<PacilmateEvent> getSpecifiedEvent(OffsetDateTime dateTime, Long userId) {

        PacilmateUser pacilmateUser = userRepository.findByUserId(userId);
        List<PacilmateEvent> events = new ArrayList<>();
        List<Integer> idEvents = new ArrayList<>();
        List<PacilmateCalendar> calendars = pacilmateUser.getSubscribedCalendar();

        int targetDate = dateTime.getDayOfYear();
        int targetYear = dateTime.getYear();

        for (PacilmateCalendar cal : calendars) {
            for (PacilmateEvent ev : cal.getEventList()) {
                if (!idEvents.contains(ev.getEventId())
                    && (ev.getTime().getDayOfYear() == targetDate)
                    && (ev.getTime().getYear() == targetYear)) {
                    idEvents.add(ev.getEventId());
                    events.add(ev);
                }
            }
        }

        return events;
    }
}
