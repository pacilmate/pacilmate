package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface LookupCalendarService {

    List<PacilmateCalendar> getListMyCalendar(Long userId);

    List<PacilmateCalendar> getListSubscribedCalendar(Long userId);
}
