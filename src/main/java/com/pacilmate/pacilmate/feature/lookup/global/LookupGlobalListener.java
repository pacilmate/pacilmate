package com.pacilmate.pacilmate.feature.lookup.global;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class LookupGlobalListener extends ListenerAdapter {

    @Autowired
    private LookupGlobalMediator lookupGlobalMediator;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();
        MessageChannel channel = event.getChannel();

        if (pacilmateUtility.isQuery(rawContent, "listcal")) {
            Message returnMessage = lookupGlobalMediator.getListCalendar(rawContent, author);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "mycal")) {
            Message returnMessage = lookupGlobalMediator.getListMyCalendar(rawContent, author);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "myev")) {
            Message returnMessage = lookupGlobalMediator.getListMyEvent(rawContent, author);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "today")) {
            Message returnMessage = lookupGlobalMediator.getListEvent("today", author);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "tmrw")) {
            Message returnMessage = lookupGlobalMediator.getListEvent("tomorrow", author);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
