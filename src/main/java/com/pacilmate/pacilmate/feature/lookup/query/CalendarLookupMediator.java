package com.pacilmate.pacilmate.feature.lookup.query;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface CalendarLookupMediator {
    Message listSpecifiedEvent(User author, String command);

    Message listFirstEvent(User author, String command);

}


