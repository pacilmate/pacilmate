package com.pacilmate.pacilmate.feature.edit;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface EditMediator {
    Message editCalendar(User author, String command);

    Message editEvent(User author, String command);

    Message deleteCalendar(User author, String command);

    Message deleteEvent(User author, String command);
}
