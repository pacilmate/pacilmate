package com.pacilmate.pacilmate.feature.edit;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class EditListener extends ListenerAdapter {

    @Autowired
    EditMediator editMediator;

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        String rawContent = message.getContentRaw();
        User author = message.getAuthor();

        MessageChannel channel = event.getChannel();
        if (pacilmateUtility.isQuery(rawContent, "editcal")) {
            Message returnMessage = editMediator.editCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "editev")) {
            Message returnMessage = editMediator.editEvent(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "delcal")) {
            Message returnMessage = editMediator.deleteCalendar(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        } else if (pacilmateUtility.isQuery(rawContent, "delev")) {
            Message returnMessage = editMediator.deleteEvent(author, rawContent);
            channel.sendMessage(returnMessage).queue();
        }
    }
}
