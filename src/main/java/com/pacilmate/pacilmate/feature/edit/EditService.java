package com.pacilmate.pacilmate.feature.edit;

import com.pacilmate.pacilmate.exception.PacilmateUnauthorizedEditException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import java.time.OffsetDateTime;
import org.springframework.stereotype.Service;

@Service
public interface EditService {
    PacilmateCalendar editCalendar(long userId, int calendarId, String title)
        throws PacilmateUnauthorizedEditException;

    PacilmateEvent editEvent(long userId, int eventId, String description,
                             OffsetDateTime eventTime) throws PacilmateUnauthorizedEditException;

    void deleteCalendar(long userId, int calendarId) throws PacilmateUnauthorizedEditException;

    void deleteEvent(long userId, int eventId) throws PacilmateUnauthorizedEditException;
}
