package com.pacilmate.pacilmate.feature.edit;

import com.pacilmate.pacilmate.exception.PacilmateUnauthorizedEditException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import java.time.OffsetDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditServiceImpl implements EditService {

    @Autowired
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Autowired
    PacilmateEventRepository pacilmateEventRepository;

    private PacilmateCalendar grabAndVerifyCalendar(long userId, int calendarId)
        throws PacilmateUnauthorizedEditException {
        PacilmateCalendar pacilmateCalendar =
            pacilmateCalendarRepository.findByCalendarId(calendarId);
        if (pacilmateCalendar == null || pacilmateCalendar.getAuthor().getUserId() != userId) {
            throw new PacilmateUnauthorizedEditException("You aren't allowed to edit!");
        }
        return pacilmateCalendar;
    }

    private PacilmateEvent grabAndVerifyEvent(long userId, int eventId)
        throws PacilmateUnauthorizedEditException {
        PacilmateEvent pacilmateEvent =
            pacilmateEventRepository.findByEventId(eventId);
        if (pacilmateEvent == null || pacilmateEvent.getAuthor().getUserId() != userId) {
            throw new PacilmateUnauthorizedEditException("You aren't allowed to edit!");
        }
        return pacilmateEvent;
    }

    @Override
    public PacilmateCalendar editCalendar(long userId, int calendarId, String title)
        throws PacilmateUnauthorizedEditException {
        PacilmateCalendar pacilmateCalendar = grabAndVerifyCalendar(userId, calendarId);
        pacilmateCalendar.setDescription(title);
        return pacilmateCalendarRepository.save(pacilmateCalendar);
    }

    @Override
    public PacilmateEvent editEvent(long userId, int eventId, String description,
                                    OffsetDateTime eventTime)
        throws PacilmateUnauthorizedEditException {
        PacilmateEvent pacilmateEvent = grabAndVerifyEvent(userId, eventId);
        pacilmateEvent.setDescription(description);
        pacilmateEvent.setTime(eventTime);
        return pacilmateEventRepository.save(pacilmateEvent);
    }

    @Override
    public void deleteCalendar(long userId, int calendarId)
        throws PacilmateUnauthorizedEditException {
        PacilmateCalendar pacilmateCalendar = grabAndVerifyCalendar(userId, calendarId);
        pacilmateCalendarRepository.delete(pacilmateCalendar);
    }

    @Override
    public void deleteEvent(long userId, int eventId)
        throws PacilmateUnauthorizedEditException {
        PacilmateEvent pacilmateEvent = grabAndVerifyEvent(userId, eventId);
        pacilmateEventRepository.delete(pacilmateEvent);
    }
}
