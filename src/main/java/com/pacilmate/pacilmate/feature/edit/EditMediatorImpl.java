package com.pacilmate.pacilmate.feature.edit;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.exception.PacilmateUnauthorizedEditException;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import io.micrometer.core.annotation.Timed;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditMediatorImpl implements EditMediator {

    @Autowired
    PacilmateUtility pacilmateUtility;

    @Autowired
    EditService editService;

    @Autowired
    PacilmateCalendarEmbedBuilder pacilmateCalendarEmbedBuilder;

    @Autowired
    PacilmateEventEmbedBuilder pacilmateEventEmbedBuilder;

    @Autowired
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @Timed("edit_calendar")
    @Override
    public Message editCalendar(User author, String command) {

        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 2);
            PacilmateCalendar pacilmateCalendar =
                editService.editCalendar(
                    author.getIdLong(),
                    Integer.parseInt(arguments.get(0)),
                    arguments.get(1)
                );
            messageBuilder.setContent(
                String.format("Calendar **%s** has been successfully edited!", arguments.get(1)));
            messageBuilder.setEmbed(pacilmateCalendarEmbedBuilder.makeEmbed(pacilmateCalendar));
        } catch (PacilmateUnverifiedCommandException | PacilmateUnauthorizedEditException e) {
            messageBuilder.setContent(e.getMessage());
        }

        return messageBuilder.build();
    }

    @Timed("edit_event")
    @Override
    public Message editEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 4);
            OffsetDateTime dateTime =
                pacilmateDateTimeParser.parse(arguments.get(2), arguments.get(3));
            PacilmateEvent pacilmateEvent = editService.editEvent(
                author.getIdLong(),
                Integer.parseInt(arguments.get(0)),
                arguments.get(1),
                dateTime
            );
            messageBuilder.setContent(
                String.format("The event **%s** has been successfully edited!", arguments.get(1)));
            messageBuilder.setEmbed(pacilmateEventEmbedBuilder.makeEmbed(pacilmateEvent));
        } catch (PacilmateUnverifiedCommandException
            | PacilmateParseDateTimeException
            | DateTimeParseException
            | PacilmateUnauthorizedEditException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();

    }

    @Timed("delete_calendar")
    @Override
    public Message deleteCalendar(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 1);
            editService.deleteCalendar(author.getIdLong(), Integer.parseInt(arguments.get(0)));
            messageBuilder.setContent("Calendar has been deleted!");
        } catch (PacilmateUnverifiedCommandException | PacilmateUnauthorizedEditException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }

    @Timed("delete_event")
    @Override
    public Message deleteEvent(User author, String command) {
        MessageBuilder messageBuilder = new MessageBuilder();
        try {
            List<String> arguments = pacilmateUtility.verify(author, command, 1);
            editService.deleteEvent(author.getIdLong(), Integer.parseInt(arguments.get(0)));
            messageBuilder.setContent("Event has been deleted!");
        } catch (PacilmateUnverifiedCommandException | PacilmateUnauthorizedEditException e) {
            messageBuilder.setContent(e.getMessage());
        }
        return messageBuilder.build();
    }
}
