package com.pacilmate.pacilmate.feature.help;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class HelpListener extends ListenerAdapter {

    private final MessageEmbed helpMessageEmbed;

    @Autowired
    private PacilmateUtility pacilmateUtility;

    public HelpListener() {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        embedBuilder.setDescription("<a:getSomeHelp:841549096351563777> **Help:**");

        embedBuilder.addField(":heart: **Global Lookup**",
            "`!listcal`: List all of your subscribed calendars.\n"
                + "`!mycal`: List all of your made calendars.\n"
                + "`!myev`: List all of your made events.\n"
                + "`!today`: List today's events.\n"
                + "`!tmrw`: List tomorrow's events.", false);

        embedBuilder.addField(":orange_heart: **Query Lookup**",
            "`!ev, {number}`: List the first {number} future events.\n"
                + "`!evdate, {date_parse}`: List events specified in the {date_parse}.",
            false);

        embedBuilder.addField(":yellow_heart: **Builder**\n",
            "`!makecal, {title}`: Create a calendar.\n"
                + "`!makeev, {description}, {time_parse}, {date_parse}`: Create an event.",
            false);

        embedBuilder.addField(":green_heart: **Edit**\n",
            "`!editcal, {calendar_id}, {title}`: Edit calendar.\n"
                + "`!editev, {event_id}, {description}, {time_parse}, "
                + "{date_parse}`: Edit event.\n"
                + "`!delcal, {calendar_id}`: Delete calendar.\n"
                + "`!delev, {event_id}`: Delete event.", false);

        embedBuilder.addField(":blue_heart: **Manage Calendar**",
            "`!sub, {calendar_id}`: Subscribe calendar with specified id.\n"
                + "`!unsub, {calendar_id}`: Unsubscribe calendar with specified id.", false);

        embedBuilder.addField(":purple_heart: **Manage Events**",
            "`!set, {calendar_id}, {event_id}`: Add event to your made calendar.\n"
                + "`!unset, {calendar_id}, {event_id}`: Remove event to your made calendar.",
            false);

        embedBuilder.addField(":white_heart: **Miscellaneous**",
            "`!remindme, {duration}`: Create a countdown timer with duration in minutes.",
            false);

        embedBuilder.addField(":brown_heart: **Judge**",
            "`!problems`: Get all problems title.\n"
                + "`!init`: Initialize yourself into **PacilJudge**.\n"
                + "`!solve, {problem_id}`: Try to solve this question.\n"
                + "`!release`: Surrender answering this question.\n"
                + "`!answer, {answer}`: Answer this question.\n"
                + "`!profile`: Check your own profile.\n"
                + "`!scoreboard`: Check scoreboard.", false);

        helpMessageEmbed = embedBuilder.build();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if (pacilmateUtility.isQuery(message.getContentRaw(), "help")) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage(helpMessageEmbed).queue();
        }
    }

}
