package com.pacilmate.pacilmate.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "pacilmate_user")
@Data
@NoArgsConstructor
public class PacilmateUser {

    @Id
    @Column(name = "id", updatable = false)
    private Long userId;

    @Column(name = "name")
    private String name;

    @Column(name = "discriminator")
    private String discriminator;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @ManyToMany(mappedBy = "subscriber", targetEntity = PacilmateCalendar.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonBackReference
    private List<PacilmateCalendar> subscribedCalendar;

    @OneToMany(mappedBy = "author", targetEntity = PacilmateCalendar.class)
    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<PacilmateCalendar> madeCalendar;

    @OneToMany(mappedBy = "author", targetEntity = PacilmateEvent.class)
    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<PacilmateEvent> madeEvent;

    public PacilmateUser(User author) {
        this.userId = author.getIdLong();
        this.name = author.getName();
        this.discriminator = author.getDiscriminator();
        this.avatarUrl = author.getEffectiveAvatarUrl();
    }

    @Override
    public String toString() {
        return String.format("UserID: %d%nname: %s%ndiscriminator: %s%navatarUrl: %s",
            userId, name, discriminator, avatarUrl);
    }


}
