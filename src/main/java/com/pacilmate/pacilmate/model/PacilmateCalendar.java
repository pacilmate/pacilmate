package com.pacilmate.pacilmate.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "pacilmate_calendar")
@Data
@NoArgsConstructor
public class PacilmateCalendar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    private Integer calendarId;

    @Column(name = "description")
    private String description;

    @Column(name = "color")
    private String color;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonManagedReference
    private List<PacilmateUser> subscriber;

    @ManyToMany(mappedBy = "calendarList", targetEntity = PacilmateEvent.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonBackReference
    private List<PacilmateEvent> eventList;

    @ManyToOne
    @JoinColumn(name = "author")
    @JsonManagedReference
    private PacilmateUser author;

    public PacilmateCalendar(String description, PacilmateUser author, String color) {
        this.description = description;
        this.author = author;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("CalendarID: %d%ndescription: %s%nauthor: %d%ncolor: %s",
            calendarId, description, author.getUserId(), color);
    }

}
