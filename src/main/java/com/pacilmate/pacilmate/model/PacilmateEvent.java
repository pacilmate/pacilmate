package com.pacilmate.pacilmate.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.time.OffsetDateTime;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "pacilmate_event")
@Data
@NoArgsConstructor
public class PacilmateEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    private Integer eventId;

    @Column(name = "description")
    private String description;

    @Column(name = "date")
    private OffsetDateTime time;

    @Column(name = "color")
    private String color;

    @ManyToMany(targetEntity = PacilmateCalendar.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonManagedReference
    private List<PacilmateCalendar> calendarList;

    @ManyToOne
    @JoinColumn(name = "author")
    @JsonManagedReference
    private PacilmateUser author;

    public PacilmateEvent(String description, OffsetDateTime time, PacilmateUser author,
                          String color) {
        this.description = description;
        this.time = time;
        this.author = author;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("EventID: %d%ndescription: %s%ntime: %s%nauthor: %d%ncolor: %s",
            eventId, description, time.toString(), author.getUserId(), color);
    }
}
