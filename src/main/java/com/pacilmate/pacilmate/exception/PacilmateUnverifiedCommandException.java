package com.pacilmate.pacilmate.exception;

public class PacilmateUnverifiedCommandException extends Exception {
    public PacilmateUnverifiedCommandException() {
    }

    public PacilmateUnverifiedCommandException(String errorMessage) {
        super(errorMessage);
    }
}
