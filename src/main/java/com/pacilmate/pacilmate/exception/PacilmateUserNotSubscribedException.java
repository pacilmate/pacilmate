package com.pacilmate.pacilmate.exception;

public class PacilmateUserNotSubscribedException extends Exception {
    public PacilmateUserNotSubscribedException() {
    }

    public PacilmateUserNotSubscribedException(String errorMessage) {
        super(errorMessage);
    }
}
