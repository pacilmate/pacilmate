package com.pacilmate.pacilmate.exception;

public class PacilmateParseDateTimeException extends Exception {
    public PacilmateParseDateTimeException() {
    }

    public PacilmateParseDateTimeException(String errorMessage) {
        super(errorMessage);
    }
}
