package com.pacilmate.pacilmate.exception;

public class PacilmateUserAlreadySubscribedException extends Exception {
    public PacilmateUserAlreadySubscribedException() {
    }

    public PacilmateUserAlreadySubscribedException(String errorMessage) {
        super(errorMessage);
    }
}
