package com.pacilmate.pacilmate.exception;

public class PacilmateUnauthorizedEditException extends Exception {
    public PacilmateUnauthorizedEditException() {
    }

    public PacilmateUnauthorizedEditException(String errorMessage) {
        super(errorMessage);
    }
}
