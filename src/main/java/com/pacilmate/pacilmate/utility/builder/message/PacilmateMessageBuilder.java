package com.pacilmate.pacilmate.utility.builder.message;

import org.springframework.stereotype.Service;

@Service
public interface PacilmateMessageBuilder<T> {
    String makeMessage(T pacilmateObject);
}
