package com.pacilmate.pacilmate.utility.builder.embed;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import java.awt.*;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateGlobalEventEmbedBuilder
    implements PacilmateEmbedBuilder<List<PacilmateEvent>> {

    @Autowired
    PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    PacilmateUser author;

    @Override
    public MessageEmbed makeEmbed(List<PacilmateEvent> events) {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        StringBuilder msg = new StringBuilder();
        for (PacilmateEvent ev : events) {
            msg.append(pacilmateEventMessageBuilder.makeMessage(ev)).append("\n");
        }

        embedBuilder
            .setAuthor(String.format("%s", author.getName()),
                null, author.getAvatarUrl())
            .setColor(new Color(255, 182, 64))
            .setDescription(msg);
        return embedBuilder.build();
    }

    public void setAuthor(PacilmateUser user) {
        author = user;
    }
}
