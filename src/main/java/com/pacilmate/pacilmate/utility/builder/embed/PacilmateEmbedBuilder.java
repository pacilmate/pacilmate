package com.pacilmate.pacilmate.utility.builder.embed;

import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public interface PacilmateEmbedBuilder<T> {
    MessageEmbed makeEmbed(T pacilmateObject);
}
