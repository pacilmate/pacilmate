package com.pacilmate.pacilmate.utility.builder.embed;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeFormatter;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateEventEmbedBuilder implements PacilmateEmbedBuilder<PacilmateEvent> {

    @Autowired
    PacilmateDateTimeFormatter pacilmateDateTimeFormatter;

    @Override
    public MessageEmbed makeEmbed(PacilmateEvent pacilmateEvent) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        PacilmateUser author = pacilmateEvent.getAuthor();
        embedBuilder
            .setAuthor(String.format("%s | 🚩 %d", author.getName(), pacilmateEvent.getEventId()),
                null, author.getAvatarUrl())
            .setColor(Color.decode(pacilmateEvent.getColor()))
            .setTitle(pacilmateEvent.getDescription())
            .setDescription(pacilmateDateTimeFormatter.getDateTime(pacilmateEvent.getTime()));
        return embedBuilder.build();
    }
}
