package com.pacilmate.pacilmate.utility.builder.embed;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeFormatter;
import java.awt.*;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateManageEventEmbedBuilder implements PacilmateEmbedBuilder<PacilmateCalendar> {

    @Autowired
    PacilmateDateTimeFormatter pacilmateDateTimeFormatter;

    @Override
    public MessageEmbed makeEmbed(PacilmateCalendar calendar) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        PacilmateUser user = calendar.getAuthor();
        List<PacilmateEvent> eventList = calendar.getEventList();
        StringBuilder message = new StringBuilder();
        for (int i = 0; i < eventList.size(); i++) {
            message.append(String.format("**%s. %s** 🚩 %s%n", (i + 1),
                eventList.get(i).getDescription(), eventList.get(i).getEventId()));
            message.append(pacilmateDateTimeFormatter.getDateTime(eventList.get(i).getTime()));
            message.append("\n");
        }
        embedBuilder.setAuthor(
                String.format("%s | 🚩 %d", user.getName(), calendar.getCalendarId()),
                null, user.getAvatarUrl())
                .setColor(Color.decode(calendar.getColor()))
                .setTitle(calendar.getDescription());
        embedBuilder.setDescription(message);
        return embedBuilder.build();
    }
}
