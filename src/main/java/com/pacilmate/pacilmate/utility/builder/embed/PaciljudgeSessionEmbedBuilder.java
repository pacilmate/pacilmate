package com.pacilmate.pacilmate.utility.builder.embed;

import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PaciljudgeSessionEmbedBuilder implements PacilmateEmbedBuilder<JsonNode> {

    private static final String PROBLEM = "problem";
    private static final String PROBLEM_TITLE = "title";
    private static final String TIME_LIMIT = "timeLimit";
    private static final String PROBLEM_DESC = "question";
    private static final String EMBED_COLOR = "#96fffc";


    @Override
    public MessageEmbed makeEmbed(JsonNode pacilmateObject) {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        JsonNode problem = pacilmateObject.get(PROBLEM);
        embedBuilder.setTitle(problem.get(PROBLEM_TITLE).asText());

        StringBuilder desc = new StringBuilder();

        desc.append("❓ Question\n");
        desc.append(problem.get(PROBLEM_DESC).asText() + "\n\n");
        desc.append(String.format("⏰ Time remaining: %d seconds %n",
            pacilmateObject.get(TIME_LIMIT).asInt()));

        embedBuilder.setDescription(desc).setColor(Color.decode(EMBED_COLOR));

        return embedBuilder.build();
    }
}
