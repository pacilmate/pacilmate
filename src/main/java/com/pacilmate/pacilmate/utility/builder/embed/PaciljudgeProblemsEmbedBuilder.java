package com.pacilmate.pacilmate.utility.builder.embed;

import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PaciljudgeProblemsEmbedBuilder implements PacilmateEmbedBuilder<JsonNode> {

    private static final String TITLE = "title";
    private static final String PROBLEM_ID = "problemId";
    private static final String PROBLEMS_COLOR = "#f9b8ff";

    @Override
    public MessageEmbed makeEmbed(JsonNode pacilmateObject) {

        EmbedBuilder embedBuilder = new EmbedBuilder();

        embedBuilder.setTitle("All problems list");
        embedBuilder.setColor(Color.decode(PROBLEMS_COLOR));

        StringBuilder desc = new StringBuilder();

        pacilmateObject.forEach(currentProblem -> desc
            .append(String.format("📄 %s - 🚩 **%s**%n", currentProblem.get(TITLE).asText(),
                currentProblem.get(PROBLEM_ID).asText())));
        embedBuilder.setDescription(desc);

        return embedBuilder.build();
    }
}
