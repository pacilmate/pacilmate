package com.pacilmate.pacilmate.utility.builder.message;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateCalendarMessageBuilder implements PacilmateMessageBuilder<PacilmateCalendar> {

    @Autowired
    PacilmateDateTimeFormatter pacilmateDateTimeFormatter;

    @Override
    public String makeMessage(PacilmateCalendar pacilmateCalendar) {
        return String.format("🚩 **%d** | %s %n",
            pacilmateCalendar.getCalendarId(),
            pacilmateCalendar.getDescription());
    }
}
