package com.pacilmate.pacilmate.utility.builder.embed;

import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PaciljudgeVerdictEmbedBuilder implements PacilmateEmbedBuilder<JsonNode> {

    private static final String WA_COLOR = "#d10000";
    private static final String AC_COLOR = "#00d12d";
    private static final String SCORE = "score";
    private static final String VERDICT = "verdict";
    private static final String TIME_REMAINING = "timeRemaining";


    @Override
    public MessageEmbed makeEmbed(JsonNode pacilmateObject) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        boolean isAccepted = pacilmateObject.get(VERDICT).asText().equals("ac");
        embedBuilder.setColor(Color.decode(isAccepted ? AC_COLOR : WA_COLOR));
        StringBuilder desc = new StringBuilder();
        if (isAccepted) {
            embedBuilder.setTitle("Accepted! ✔");
            Double score = pacilmateObject.get(SCORE).asDouble();
            desc.append(String.format("💯 Score %.2f %n", score));
        } else {
            embedBuilder.setTitle("Wrong Answer! ❌");
            int timeRemaining = pacilmateObject.get(TIME_REMAINING).asInt();
            desc.append(String.format("⏰ Time remaining: %d", timeRemaining));
        }
        return embedBuilder.setDescription(desc).build();
    }
}
