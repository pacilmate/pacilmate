package com.pacilmate.pacilmate.utility.builder.embed;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateCalendarMessageBuilder;
import java.awt.*;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateGlobalCalendarEmbedBuilder
    implements PacilmateEmbedBuilder<List<PacilmateCalendar>> {

    @Autowired
    PacilmateCalendarMessageBuilder pacilmateCalendarMessageBuilder;

    PacilmateUser author;

    @Override
    public MessageEmbed makeEmbed(List<PacilmateCalendar> calendars) {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        StringBuilder msg = new StringBuilder();
        for (PacilmateCalendar cal : calendars) {
            msg.append(pacilmateCalendarMessageBuilder.makeMessage(cal)).append("\n");
        }

        embedBuilder
            .setAuthor(String.format("%s", author.getName()),
                null, author.getAvatarUrl())
            .setColor(new Color(3, 252, 232))
            .setDescription(msg);
        return embedBuilder.build();
    }

    public void setAuthor(PacilmateUser user) {
        author = user;
    }
}
