package com.pacilmate.pacilmate.utility.builder.embed;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PacilmateCalendarEmbedBuilder implements PacilmateEmbedBuilder<PacilmateCalendar> {

    @Override
    public MessageEmbed makeEmbed(PacilmateCalendar pacilmateCalendar) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        PacilmateUser author = pacilmateCalendar.getAuthor();
        embedBuilder.setAuthor(
            String.format("%s | 🚩 %d", author.getName(), pacilmateCalendar.getCalendarId()),
            null, author.getAvatarUrl())
            .setColor(Color.decode(pacilmateCalendar.getColor()))
            .setTitle(pacilmateCalendar.getDescription());
        return embedBuilder.build();
    }
}
