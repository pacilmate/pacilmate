package com.pacilmate.pacilmate.utility.builder.embed;

import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PaciljudgeUserEmbedBuilder implements PacilmateEmbedBuilder<JsonNode> {

    private static final String PROBLEMS_SOLVED = "problemsSolved";
    private static final String USERNAME = "username";
    private static final String SCORE = "score";
    private static final String PROBLEM = "problem";
    private static final String PROBLEM_ID = "problemId";
    private static final String CURRENT_SOLVE = "currentSolve";
    private static final String EMBED_COLOR = "#fcb103";

    @Override
    public MessageEmbed makeEmbed(JsonNode pacilmateObject) {

        EmbedBuilder embedBuilder = new EmbedBuilder();
        String username = pacilmateObject.get(USERNAME).asText();
        embedBuilder.setColor(Color.decode(EMBED_COLOR)).setTitle("🧠 " + username);
        double score = pacilmateObject.get(SCORE).asDouble();
        String problemStr = "An error has occurred 😭";
        try {
            JsonNode problemId = pacilmateObject.get(CURRENT_SOLVE).get(PROBLEM).get(PROBLEM_ID);
            problemStr = String.format("***%s***", problemId.asText());
        } catch (NullPointerException e) {
            problemStr = "Nothing";
        }
        StringBuilder desc = new StringBuilder();

        JsonNode problemsSolved = pacilmateObject.get(PROBLEMS_SOLVED);

        desc.append(String.format("💯 **Score**: %.2f%n", score));
        desc.append(String.format("📚 **Problems Solved**: %d%n", problemsSolved.size()));
        desc.append(String.format("✏ **Currently Solving**: %s%n", problemStr));

        embedBuilder.setDescription(desc);
        return embedBuilder.build();
    }
}
