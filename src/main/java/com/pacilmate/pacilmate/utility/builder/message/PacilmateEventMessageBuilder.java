package com.pacilmate.pacilmate.utility.builder.message;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateEventMessageBuilder implements PacilmateMessageBuilder<PacilmateEvent> {

    @Autowired
    PacilmateDateTimeFormatter pacilmateDateTimeFormatter;

    @Override
    public String makeMessage(PacilmateEvent pacilmateEvent) {
        String message = "";
        message += String.format("🚩 **%d** | %s %n",
            pacilmateEvent.getEventId(),
            pacilmateEvent.getDescription());
        message += pacilmateDateTimeFormatter.getDateTime(pacilmateEvent.getTime()) + "\n";
        return message;
    }
}
