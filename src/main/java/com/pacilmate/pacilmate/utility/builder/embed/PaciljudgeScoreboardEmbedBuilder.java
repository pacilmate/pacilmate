package com.pacilmate.pacilmate.utility.builder.embed;

import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import java.util.Iterator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.springframework.stereotype.Service;

@Service
public class PaciljudgeScoreboardEmbedBuilder implements PacilmateEmbedBuilder<JsonNode> {

    private static final String SCORE = "score";
    private static final String USERNAME = "username";
    private static final String SCOREBOARD_COLOR = "#8cfff7";

    private String getMedal(int index) {
        if (index == 1) {
            return "🥇";
        } else if (index == 2) {
            return "🥈";
        } else if (index == 3) {
            return "🥉";
        } else {
            return "🏅";
        }
    }

    @Override
    public MessageEmbed makeEmbed(JsonNode pacilmateObject) {

        EmbedBuilder embedBuilder = new EmbedBuilder();

        embedBuilder.setTitle("🏆 Top 10").setColor(Color.decode(SCOREBOARD_COLOR));

        StringBuilder desc = new StringBuilder();

        Iterator<JsonNode> jsonNodeIterator = pacilmateObject.elements();
        for (int i = 1; jsonNodeIterator.hasNext(); i++) {
            JsonNode currentUser = jsonNodeIterator.next();
            String username = currentUser.get(USERNAME).asText();
            Double score = currentUser.get(SCORE).asDouble();
            desc.append(String.format("%s %s - %.2f%n", getMedal(i), username, score));
        }

        embedBuilder.setDescription(desc);

        return embedBuilder.build();
    }
}
