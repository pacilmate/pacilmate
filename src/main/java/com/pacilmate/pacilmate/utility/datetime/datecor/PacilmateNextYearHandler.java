package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmateNextYearHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {
        if (string.equalsIgnoreCase("next year")) {
            return OffsetDateTime.now(ZoneId.of(timezone)).plusYears(1);
        }
        return null;
    }
}
