package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmateTomorrowHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {
        if (string.equalsIgnoreCase("tomorrow")) {
            return OffsetDateTime.now(ZoneId.of(timezone)).plusDays(1);
        }
        return null;
    }
}
