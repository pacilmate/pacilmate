package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class PacilmateExactDateHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {
        try {
            LocalDate localDate =
                LocalDate.parse(string, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            return OffsetDateTime.of(localDate, LocalTime.MIN, ZoneOffset.of(timezone));
        } catch (Exception e) {
            return null;
        }
    }
}
