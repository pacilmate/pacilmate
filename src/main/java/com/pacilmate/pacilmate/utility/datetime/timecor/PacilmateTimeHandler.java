package com.pacilmate.pacilmate.utility.datetime.timecor;

import java.time.Duration;

public abstract class PacilmateTimeHandler {

    private PacilmateTimeHandler nextHandler;

    public abstract Duration compute(String string, String timezone);

    /**
     * Return duration since truncated days from a duration string.
     *
     * @param string the string specified
     * @return Duration object passed since begin of date
     */
    public Duration get(String string, String timezone) {
        Duration result = compute(string, timezone);
        if (result == null && nextHandler != null) {
            return nextHandler.get(string, timezone);
        }
        return result;
    }

    public PacilmateTimeHandler setNextHandler(
        PacilmateTimeHandler nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }

}
