package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmatePlusDateHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {
        try {
            long deltaDays = Long.parseLong(string);
            return OffsetDateTime.now(ZoneId.of(timezone)).plusDays(deltaDays);
        } catch (Exception e) {
            return null;
        }
    }
}
