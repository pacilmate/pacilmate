package com.pacilmate.pacilmate.utility.datetime;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

@Service
public class PacilmateDateTimeFormatterImpl implements PacilmateDateTimeFormatter {

    private static final String TIMEZONE = "+7";

    @Override
    public String getDateTime(OffsetDateTime currentTime) {
        return getTime(currentTime) + getDate(currentTime);
    }

    @Override
    public String getDate(OffsetDateTime currentTime) {
        currentTime = currentTime.withOffsetSameInstant(ZoneOffset.of(TIMEZONE));
        return String.format(":calendar: %s%n",
            currentTime.format(DateTimeFormatter.ofPattern("EEEE, d MMMM yyyy"))
        );
    }

    @Override
    public String getTime(OffsetDateTime currentTime) {
        currentTime = currentTime.withOffsetSameInstant(ZoneOffset.of(TIMEZONE));
        return String.format(":alarm_clock: %s (GMT%s)%n",
            currentTime.format(DateTimeFormatter.ofPattern("HH:mm")),
            currentTime.format(DateTimeFormatter.ofPattern("X"))
        );
    }
}
