package com.pacilmate.pacilmate.utility.datetime;


import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import java.time.OffsetDateTime;
import org.springframework.stereotype.Service;

@Service
public interface PacilmateDateTimeParser {
    OffsetDateTime parse(String timeString, String dateString, String timezone)
        throws PacilmateParseDateTimeException;

    OffsetDateTime parse(String timeString, String dateString)
        throws PacilmateParseDateTimeException;
}
