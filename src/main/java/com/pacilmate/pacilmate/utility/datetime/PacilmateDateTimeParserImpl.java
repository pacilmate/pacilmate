package com.pacilmate.pacilmate.utility.datetime;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.utility.datetime.datecor.*;
import com.pacilmate.pacilmate.utility.datetime.timecor.PacilmatePlusHoursHandler;
import com.pacilmate.pacilmate.utility.datetime.timecor.PacilmatePlusMinutesHandler;
import com.pacilmate.pacilmate.utility.datetime.timecor.PacilmateTimeHandler;
import com.pacilmate.pacilmate.utility.datetime.timecor.PacilmateTimeHoursMinutesHandler;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class PacilmateDateTimeParserImpl implements PacilmateDateTimeParser {

    private static final String TIMEZONE = "+7";

    PacilmateDateHandler pacilmateDateHandler;
    PacilmateTimeHandler pacilmateTimeHandler;

    @PostConstruct
    public void init() {
        PacilmateDateHandler nextHandler;
        PacilmateTimeHandler nextTimeHandler;

        pacilmateDateHandler = nextHandler = new PacilmateTodayHandler();
        nextHandler = nextHandler.setNextHandler(new PacilmateTomorrowHandler());
        nextHandler = nextHandler.setNextHandler(new PacilmateNextWeekHandler());
        nextHandler = nextHandler.setNextHandler(new PacilmateNextMonthHandler());
        nextHandler = nextHandler.setNextHandler(new PacilmateNextYearHandler());
        nextHandler = nextHandler.setNextHandler(new PacilmateExactDateHandler());
        nextHandler.setNextHandler(new PacilmatePlusDateHandler());

        pacilmateTimeHandler = nextTimeHandler = new PacilmateTimeHoursMinutesHandler();
        nextTimeHandler = nextTimeHandler.setNextHandler(new PacilmatePlusHoursHandler());
        nextTimeHandler.setNextHandler(new PacilmatePlusMinutesHandler());
    }

    @Override
    public OffsetDateTime parse(String timeString, String dateString, String timezone)
        throws PacilmateParseDateTimeException {
        Duration time = pacilmateTimeHandler.get(timeString, timezone);
        if (time == null) {
            throw new PacilmateParseDateTimeException("Time can't be parsed!");
        }
        OffsetDateTime date = pacilmateDateHandler.get(dateString, timezone);
        if (date == null) {
            throw new PacilmateParseDateTimeException("Date can't be parsed");
        }
        return date.truncatedTo(ChronoUnit.DAYS).plus(time);
    }

    @Override
    public OffsetDateTime parse(String timeString, String dateString)
        throws PacilmateParseDateTimeException {
        return parse(timeString, dateString, TIMEZONE);
    }
}
