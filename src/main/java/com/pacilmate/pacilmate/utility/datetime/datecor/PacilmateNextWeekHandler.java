package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmateNextWeekHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {

        if (string.equalsIgnoreCase("next week")) {
            return OffsetDateTime.now(ZoneId.of(timezone)).plusWeeks(1);
        }
        return null;
    }
}
