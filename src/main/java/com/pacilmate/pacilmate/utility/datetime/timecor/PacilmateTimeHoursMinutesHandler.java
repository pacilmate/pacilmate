package com.pacilmate.pacilmate.utility.datetime.timecor;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class PacilmateTimeHoursMinutesHandler extends PacilmateTimeHandler {
    @Override
    public Duration compute(String string, String timezone) {
        try {
            return Duration
                .between(LocalTime.MIN,
                    LocalTime.parse(string, DateTimeFormatter.ofPattern("HH:mm")));
        } catch (Exception ignored) {

        }
        return null;
    }
}
