package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmateTodayHandler extends PacilmateDateHandler {

    @Override
    public OffsetDateTime compute(String string, String timezone) {
        if (string.equalsIgnoreCase("today")) {
            return OffsetDateTime.now(ZoneId.of(timezone));
        }
        return null;
    }
}
