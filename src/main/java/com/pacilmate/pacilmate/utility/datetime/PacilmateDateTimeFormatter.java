package com.pacilmate.pacilmate.utility.datetime;

import java.time.OffsetDateTime;
import org.springframework.stereotype.Service;

@Service
public interface PacilmateDateTimeFormatter {
    String getDateTime(OffsetDateTime currentTime);

    String getDate(OffsetDateTime currentTime);

    String getTime(OffsetDateTime currentTime);
}
