package com.pacilmate.pacilmate.utility.datetime.timecor;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class PacilmatePlusMinutesHandler extends PacilmateTimeHandler {
    @Override
    public Duration compute(String string, String timezone) {
        try {
            if (string.endsWith("m")) {

                string = string.substring(0, string.length() - 1);
                OffsetDateTime now = OffsetDateTime.now(ZoneId.of(timezone));
                return Duration.between(now.truncatedTo(
                    ChronoUnit.DAYS), now).plus(Duration.ofMinutes(Long.parseLong(string)));
            }
        } catch (Exception ignored) {
            // This would return null instead
        }
        return null;
    }
}
