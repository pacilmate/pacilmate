package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;

public abstract class PacilmateDateHandler {

    private PacilmateDateHandler nextHandler;

    public abstract OffsetDateTime compute(String string, String timezone);

    /**
     * Return day of specified choice by a string.
     *
     * @param string the string specified
     * @return OffsetDateTime object with truncated time of day
     */
    public OffsetDateTime get(String string, String timezone) {
        OffsetDateTime result = compute(string, timezone);
        if (result == null && nextHandler != null) {
            return nextHandler.get(string, timezone);
        }
        return result;
    }

    public PacilmateDateHandler setNextHandler(
        PacilmateDateHandler nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }
}
