package com.pacilmate.pacilmate.utility.datetime.datecor;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PacilmateNextMonthHandler extends PacilmateDateHandler {
    @Override
    public OffsetDateTime compute(String string, String timezone) {

        if (string.equalsIgnoreCase("next month")) {
            return OffsetDateTime.now(ZoneId.of(timezone)).plusMonths(1);
        }
        return null;
    }
}
