package com.pacilmate.pacilmate.utility;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacilmateUtilityImpl implements PacilmateUtility {

    @Autowired
    PacilmateUserRepository pacilmateUserRepository;

    @Override
    public boolean isQuery(String command, String query) {
        return command.trim().toLowerCase().startsWith("!" + query);
    }

    @Override
    public String randomColor() {
        return String.format("#%06x", (new Random()).nextInt(0xffffff + 1));
    }

    @Override
    public List<String> verify(User author, String command, int args)
        throws PacilmateUnverifiedCommandException {
        List<String> splitted = new ArrayList<>(Arrays.asList(command.trim().split("\\s*,\\s*")));
        if (splitted.size() != args + 1) {
            throw new PacilmateUnverifiedCommandException(
                "Arguments are not enough " + author.getAsMention());
        }
        splitted.remove(0);

        if (pacilmateUserRepository.findByUserId(author.getIdLong()) == null) {
            PacilmateUser pacilmateUser = new PacilmateUser(author);
            pacilmateUserRepository.save(pacilmateUser);
        }

        return splitted;
    }
}
