package com.pacilmate.pacilmate.utility;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import java.util.List;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface PacilmateUtility {
    boolean isQuery(String command, String query);

    String randomColor();

    List<String> verify(User author, String command, int args)
        throws PacilmateUnverifiedCommandException;
}
