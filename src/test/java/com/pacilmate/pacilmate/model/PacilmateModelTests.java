package com.pacilmate.pacilmate.model;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PacilmateModelTests {
    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("testCalendar", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("testEvent", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testUserModel() {
        assertTrue(pacilmateUser.toString().contains("Venti"));
    }

    @Test
    void testCalendarModel() {
        assertTrue(pacilmateCalendar.toString().contains("testCalendar"));
    }

    @Test
    void testEventModel() {
        assertTrue(pacilmateEvent.toString().contains("testEvent"));
    }
}
