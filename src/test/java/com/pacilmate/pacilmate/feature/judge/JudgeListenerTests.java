package com.pacilmate.pacilmate.feature.judge;

import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JudgeListenerTests {

    @Mock
    private JudgeMediator judgeMediator;

    @Spy
    private PacilmateUtilityImpl pacilmateUtility;

    @InjectMocks
    private JudgeListener judgeListener;

    @Mock
    private MessageReceivedEvent messageReceivedEvent;
    @Mock
    private Message message;
    @Mock
    private User author;
    @Mock
    private MessageChannel channel;
    @Mock
    private MessageAction afterSend;
    @Mock
    private RestAction<Void> afterReact;
    @Mock
    private Message responseMessage;

    @BeforeEach
    public void setup() {
        lenient().when(messageReceivedEvent.getMessage()).thenReturn(message);
        lenient().when(message.getAuthor()).thenReturn(author);
        lenient().when(messageReceivedEvent.getChannel()).thenReturn(channel);
        lenient().when(channel.sendMessage(anyString())).thenReturn(afterSend);

        lenient().when(responseMessage.addReaction(anyString())).thenReturn(afterReact);
        lenient().when(responseMessage.editMessage(any(Message.class))).thenReturn(afterSend);
        lenient().when(afterSend.override(anyBoolean())).thenReturn(afterSend);
        lenient().when(responseMessage.clearReactions()).thenReturn(afterReact);
    }

    @ParameterizedTest
    @ValueSource(strings = {"init", "problems", "solve", "release", "answer", "profile",
        "scoreboard"})
    void testValidQueriesOk(String query) {
        query = "!" + query;
        when(message.getContentRaw()).thenReturn(query);

        judgeListener.onMessageReceived(messageReceivedEvent);
        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(channel, times(1)).sendMessage(anyString());
        verify(messageReceivedEvent, times(1)).getMessage();

        verify(pacilmateUtility, atLeastOnce()).isQuery(anyString(), anyString());
    }

    @Test
    void testGiberrishOk() {
        String query = "!sasdasd";
        when(message.getContentRaw()).thenReturn(query);

        judgeListener.onMessageReceived(messageReceivedEvent);
        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(channel, times(0)).sendMessage(anyString());
        verify(messageReceivedEvent, times(1)).getMessage();
    }

    @Test
    void testResponseMessageGetProfileOk() {
        when(judgeMediator.checkProfile(any(User.class))).thenReturn(responseMessage);
        judgeListener.profileAccepted(responseMessage, author);
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).checkProfile(any(User.class));
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();

    }

    @Test
    void testResponseMessageInitProfileOk() {
        when(judgeMediator.initUser(any(User.class))).thenReturn(responseMessage);
        judgeListener.initAccepted(responseMessage, author);
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).initUser(any(User.class));
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();

    }

    @Test
    void testResponseMessageReleaseOk() {
        when(judgeMediator.releaseProblem(any(User.class))).thenReturn(responseMessage);
        judgeListener.releaseAccepted(responseMessage, author);
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).releaseProblem(any(User.class));
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();

    }

    @Test
    void testResponseMessageGetProblemsOk() {
        when(judgeMediator.getAllProblems()).thenReturn(responseMessage);
        judgeListener.problemAccepted(responseMessage);
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).getAllProblems();
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();
    }

    @Test
    void testResponseMessageGetScoreboardOk() {
        when(judgeMediator.scoreboard()).thenReturn(responseMessage);
        judgeListener.scoreboardAccepted(responseMessage);
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).scoreboard();
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();
    }

    @Test
    void testResponseMessageSolveProblemsOk() {
        when(judgeMediator.solveProblem(any(User.class), anyString())).thenReturn(responseMessage);
        judgeListener.solveAccepted(responseMessage, author, "asdsa");
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).solveProblem(any(User.class), anyString());
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();
    }

    @Test
    void testResponseMessageAnswerProblemsOk() {
        when(judgeMediator.answerProblem(any(User.class), anyString())).thenReturn(responseMessage);
        judgeListener.answerAccepted(responseMessage, author, "asdsa");
        verifyNoInteractions(channel, message);
        verify(responseMessage, times(1)).addReaction(anyString());
        verify(judgeMediator, times(1)).answerProblem(any(User.class), anyString());
        verify(responseMessage, times(1)).editMessage(any(Message.class));
        verify(responseMessage, times(1)).clearReactions();
    }
}
