package com.pacilmate.pacilmate.feature.judge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClientRequestException;

@ExtendWith(MockitoExtension.class)
class JudgeMediatorTests {

    private final ObjectMapper mapper = new ObjectMapper();
    @Mock
    PacilmateUtility pacilmateUtility;
    @Mock
    JudgeService judgeService;
    @Spy
    PaciljudgeUserEmbedBuilder paciljudgeUserEmbedBuilder;
    @Spy
    PaciljudgeScoreboardEmbedBuilder paciljudgeScoreboardEmbedBuilder;
    @Spy
    PaciljudgeSessionEmbedBuilder paciljudgeSessionEmbedBuilder;
    @Spy
    PaciljudgeProblemsEmbedBuilder paciljudgeProblemsEmbedBuilder;
    @Spy
    PaciljudgeVerdictEmbedBuilder paciljudgeVerdictEmbedBuilder;
    @InjectMocks
    JudgeMediatorImpl judgeMediator;

    @Mock
    private User user;

    @BeforeEach
    void setup() {
        lenient().when(user.getIdLong()).thenReturn(6969420L);
        lenient().when(user.getName()).thenReturn("hocky");
    }

    @Test
    void testGetAllProblemsOk() throws JsonProcessingException {
        JsonNode response = mapper.readTree("[\n"
            + "    {\n"
            + "        \"problemId\" : \"MATH\",\n"
            + "        \"title\" : \"Your problem title\",\n"
            + "        \"question\" : \"What is 1 + 1?\",\n"
            + "        \"answer\" : \"2\"\n"
            + "    }\n"
            + "]\n");
        when(judgeService.getProblems()).thenReturn(response);

        Message returnMessage = judgeMediator.getAllProblems();

        assertEquals("All problems list", returnMessage.getEmbeds().get(0).getTitle());
    }

    @Test
    void testScoreboardMediatorIsOk() throws JsonProcessingException {

        JsonNode response = mapper.readTree("[\n"
            + "    {\n"
            + "        \"userId\": 69694202,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 79.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 6969420,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 19.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 69694208,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 5.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 69694211,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 4.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    }\n"
            + "]\n");
        when(judgeService.scoreboard()).thenReturn(response);

        Message returnMessage = judgeMediator.scoreboard();

        assertEquals("🏆 Top 10", returnMessage.getEmbeds().get(0).getTitle());
    }

    @Test
    void testInitUserIsOk() throws JsonProcessingException {
        JsonNode response = mapper.readTree("{\n"
            + "    \"userId\": 6969420,\n"
            + "    \"username\": \"hocky\",\n"
            + "    \"score\": 0.0,\n"
            + "    \"problemsSolved\": [],\n"
            + "    \"currentSolve\": null,\n"
            + "    \"status\": 200\n"
            + "}\n");
        when(judgeService.initUser(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.initUser(user);
        assertTrue(
            Objects.requireNonNull(returnMessage.getEmbeds().get(0).getTitle()).contains("🧠"));
    }

    @Test
    void testCheckProfileIsOk() throws JsonProcessingException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"userId\": 6969420,\n"
            + "    \"username\": \"hocky\",\n"
            + "    \"score\": 0.0,\n"
            + "    \"problemsSolved\": [],\n"
            + "    \"currentSolve\": null,\n"
            + "    \"status\": 200\n"
            + "}\n");
        when(judgeService.checkUser(anyLong())).thenReturn(response);
        Message returnMessage = judgeMediator.checkProfile(user);
        assertTrue(
            Objects.requireNonNull(returnMessage.getEmbeds().get(0).getTitle()).contains("🧠"));
    }

    @Test
    void testCheckProfileIsOkNot200() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"status\": 400\n"
            + "}\n");
        when(judgeService.checkUser(anyLong())).thenReturn(response);

        Message returnMessage = judgeMediator.checkProfile(user);
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("Please do init"));
    }

    @Test
    void testCheckProfilePaciljudgeOffline() throws JsonProcessingException {

        when(judgeService.checkUser(anyLong()))
            .thenThrow(
                new WebClientRequestException(new NullPointerException("lol"), HttpMethod.GET,
                    URI.create("Yadayada"), HttpHeaders.EMPTY));
        Message returnMessage = judgeMediator.checkProfile(user);
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("PacilJudge is Offline!"));
    }


    @Test
    void testSolveProblemIsOk() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"sessionId\": 123123123123,\n"
            + "    \"problem\": {\n"
            + "        \"problemId\": \"Cp\",\n"
            + "        \"title\": \"Ini judul 2\",\n"
            + "        \"question\": \"empat tambah satu berapa?\",\n"
            + "        \"answer\": \"lima\",\n"
            + "        \"solvedBy\": []\n"
            + "    },\n"
            + "    \"endsAt\": {\n"
            + "        \"nano\": 913658400,\n"
            + "        \"year\": 2021,\n"
            + "        \"monthValue\": 5,\n"
            + "        \"dayOfMonth\": 15,\n"
            + "        \"hour\": 11,\n"
            + "        \"minute\": 8,\n"
            + "        \"second\": 46,\n"
            + "        \"month\": \"MAY\",\n"
            + "        \"dayOfWeek\": \"SATURDAY\",\n"
            + "        \"dayOfYear\": 135,\n"
            + "        \"chronology\": {\n"
            + "            \"id\": \"ISO\",\n"
            + "            \"calendarType\": \"iso8601\"\n"
            + "        }\n"
            + "    },\n"
            + "    \"tries\": 0,\n"
            + "    \"timeLimit\": 300,\n"
            + "    \"message\": \"OK\",\n"
            + "    \"status\": 200\n"
            + "}\n");
        List<String> arguments = new ArrayList<>();
        arguments.add("Cp");
        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1))).thenReturn(arguments);
        when(judgeService.solveProblem(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.solveProblem(user, "Cp");
        assertEquals("Ini judul 2", returnMessage.getEmbeds().get(0).getTitle());
    }

    @Test
    void testSolveProblemIsOkNot200() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"tries\": 0,\n"
            + "    \"timeLimit\": 300,\n"
            + "    \"message\": \"This is the message\",\n"
            + "    \"status\": 404\n"
            + "}\n");
        List<String> arguments = new ArrayList<>();
        arguments.add("Cp");
        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1))).thenReturn(arguments);
        when(judgeService.solveProblem(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.solveProblem(user, "Cp");
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("This is the message"));
    }

    @Test
    void testSolveProblemUnverified() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1)))
            .thenThrow(new PacilmateUnverifiedCommandException("Yada yada"));
        Message returnMessage = judgeMediator.solveProblem(user, "Cp");
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("Yada yada"));
    }

    @Test
    void testAnswerProblemIsOk() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"timeRemaining\": 295.0,\n"
            + "    \"verdict\": \"ac\",\n"
            + "    \"score\": 98.66666666666667,\n"
            + "    \"status\": 200\n"
            + "}\n");

        List<String> arguments = new ArrayList<>();
        arguments.add("Yes");
        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1))).thenReturn(arguments);
        when(judgeService.answerProblem(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.answerProblem(user, "Cp");
        assertEquals("Accepted! ✔", returnMessage.getEmbeds().get(0).getTitle());
    }

    @Test
    void testAnswerProblemIsWrongAnswer() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"timeRemaining\": 296.0,\n"
            + "    \"verdict\": \"wa\",\n"
            + "    \"status\": 200\n"
            + "}\n");

        List<String> arguments = new ArrayList<>();
        arguments.add("Yes");
        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1))).thenReturn(arguments);
        when(judgeService.answerProblem(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.answerProblem(user, "Cp");
        assertEquals("Wrong Answer! ❌", returnMessage.getEmbeds().get(0).getTitle());
    }

    @Test
    void testAnswerProblemIsOkNot200() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"status\": 404\n"
            + "}\n");
        List<String> arguments = new ArrayList<>();
        arguments.add("yada yada");
        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1))).thenReturn(arguments);
        when(judgeService.answerProblem(anyLong(), anyString())).thenReturn(response);
        Message returnMessage = judgeMediator.answerProblem(user, "yada yada");
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("No currently active sessions"));
    }

    @Test
    void testAnswerProblemUnverified() throws JsonProcessingException,
        PacilmateUnverifiedCommandException {

        when(pacilmateUtility.verify(any(User.class), anyString(), eq(1)))
            .thenThrow(new PacilmateUnverifiedCommandException("Yada yada"));
        Message returnMessage = judgeMediator.answerProblem(user, "Cp");
        assertTrue(returnMessage.getEmbeds().isEmpty());
        assertTrue(returnMessage.getContentRaw().contains("Yada yada"));
    }

    @ParameterizedTest
    @CsvSource({
        "200,Your currently running problem has been released!",
        "204,You don't have any running problem!",
        "404,Please do init"
    })
    void testReleaseProblemIsOk(Integer status, String verdict) {
        when(judgeService.releaseProblem(anyLong())).thenReturn(status);
        Message returnMessage = judgeMediator.releaseProblem(user);
        assertTrue(returnMessage.getContentRaw()
            .contains(verdict));
    }

    @Test
    void testCheckProfileSolvingIsOk() throws JsonProcessingException {

        JsonNode response = mapper.readTree("{\n"
            + "    \"userId\": 6969420,\n"
            + "    \"username\": \"hocky\",\n"
            + "    \"score\": 0.0,\n"
            + "    \"problemsSolved\": [],\n"
            + "    \"currentSolve\": {\n"
            + "    \"sessionId\": 123123123123,\n"
            + "    \"problem\": {\n"
            + "        \"problemId\": \"Cp\",\n"
            + "        \"title\": \"Ini judul 2\",\n"
            + "        \"question\": \"empat tambah satu berapa?\",\n"
            + "        \"answer\": \"lima\",\n"
            + "        \"solvedBy\": []\n"
            + "    },\n"
            + "    \"endsAt\": {\n"
            + "        \"nano\": 913658400,\n"
            + "        \"year\": 2021,\n"
            + "        \"monthValue\": 5,\n"
            + "        \"dayOfMonth\": 15,\n"
            + "        \"hour\": 11,\n"
            + "        \"minute\": 8,\n"
            + "        \"second\": 46,\n"
            + "        \"month\": \"MAY\",\n"
            + "        \"dayOfWeek\": \"SATURDAY\",\n"
            + "        \"dayOfYear\": 135,\n"
            + "        \"chronology\": {\n"
            + "            \"id\": \"ISO\",\n"
            + "            \"calendarType\": \"iso8601\"\n"
            + "        }\n"
            + "    },\n"
            + "    \"tries\": 0,\n"
            + "    \"timeLimit\": 300,\n"
            + "    \"message\": \"OK\",\n"
            + "    \"status\": 200\n"
            + "},\n"
            + "    \"status\": 200\n"
            + "}\n");
        when(judgeService.checkUser(anyLong())).thenReturn(response);
        Message returnMessage = judgeMediator.checkProfile(user);
        assertTrue(
            Objects.requireNonNull(returnMessage.getEmbeds().get(0).getTitle()).contains("🧠"));
    }
}
