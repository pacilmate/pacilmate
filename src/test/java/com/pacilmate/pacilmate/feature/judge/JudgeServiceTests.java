package com.pacilmate.pacilmate.feature.judge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Objects;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JudgeServiceTests {

    private static final String PROBLEM_URL = "/view";
    private static final String SCOREBOARD_URL = "/scoreboard";
    private static final String SOLVE_URL = "/solve";
    private static final String USER_URL = "/user";
    private static final String INIT_URL = "/init";
    private static final String ANSWER_URL = "/answer";
    private static final String RELEASE_URL = "/release";

    private static final String USER_ID = "userId";
    private static final String USERNAME = "username";

    private final WebClient.Builder webClientBuilder = mock(WebClient.Builder.class);
    private JudgeServiceImpl judgeService;
    private MockWebServer mockWebServer;
    private WebClient webClient;

    @BeforeAll
    void setUpAll() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        judgeService = new JudgeServiceImpl(webClientBuilder);
        webClient = WebClient.create(String.format("http://localhost:%s",
            mockWebServer.getPort()));
    }

    @BeforeEach
    void setUp() {
        when(webClientBuilder.baseUrl(nullable(String.class)))
            .thenReturn(webClientBuilder);
        when(webClientBuilder.build()).thenReturn(webClient);
    }

    @AfterAll
    void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void testGetProblemIsGood() throws InterruptedException {
        mockWebServer.enqueue(new MockResponse().setBody("[{\"test\" : \"OK\"}]")
            .addHeader("Content-Type", "application/json"));
        JsonNode response = judgeService.getProblems();
        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("GET", recordedRequest.getMethod());

        assertEquals(PROBLEM_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        assertEquals(1, response.size());
    }

    @Test
    void testGetScoreboardIsGood() throws InterruptedException {
        mockWebServer.enqueue(new MockResponse().setBody("[\n"
            + "    {\n"
            + "        \"userId\": 69694202,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 79.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 6969420,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 19.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 69694208,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 5.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    },\n"
            + "    {\n"
            + "        \"userId\": 69694211,\n"
            + "        \"username\": \"hocky\",\n"
            + "        \"score\": 4.0,\n"
            + "        \"problemsSolved\": [],\n"
            + "        \"currentSolve\": null\n"
            + "    }\n"
            + "]\n")
            .addHeader("Content-Type", "application/json"));
        JsonNode response = judgeService.scoreboard();
        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("GET", recordedRequest.getMethod());

        assertEquals(SCOREBOARD_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        assertEquals(4, response.size());

    }

    @Test
    void testServiceSolveProblem() throws InterruptedException, JsonProcessingException {

        mockWebServer.enqueue(new MockResponse().setBody("{\n"
            + "    \"sessionId\": 123123123123,\n"
            + "    \"problem\": {\n"
            + "        \"problemId\": \"Cp\",\n"
            + "        \"title\": \"Ini judul 2\",\n"
            + "        \"question\": \"empat tambah satu berapa?\",\n"
            + "        \"answer\": \"lima\",\n"
            + "        \"solvedBy\": []\n"
            + "    },\n"
            + "    \"endsAt\": {\n"
            + "        \"nano\": 913658400,\n"
            + "        \"year\": 2021,\n"
            + "        \"monthValue\": 5,\n"
            + "        \"dayOfMonth\": 15,\n"
            + "        \"hour\": 11,\n"
            + "        \"minute\": 8,\n"
            + "        \"second\": 46,\n"
            + "        \"month\": \"MAY\",\n"
            + "        \"dayOfWeek\": \"SATURDAY\",\n"
            + "        \"dayOfYear\": 135,\n"
            + "        \"chronology\": {\n"
            + "            \"id\": \"ISO\",\n"
            + "            \"calendarType\": \"iso8601\"\n"
            + "        }\n"
            + "    },\n"
            + "    \"tries\": 0,\n"
            + "    \"timeLimit\": 300,\n"
            + "    \"message\": \"OK\",\n"
            + "    \"status\": 200\n"
            + "}\n")
            .addHeader("Content-Type", "application/json"));

        JsonNode response = judgeService.solveProblem(135363333330042880L, "Cp");

        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("POST", recordedRequest.getMethod());

        assertEquals(SOLVE_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode request = objectMapper.readTree(recordedRequest.getBody().readUtf8());

        assertEquals(135363333330042880L, request.get(USER_ID).asLong());

        assertEquals(123123123123L, response.get("sessionId").asLong());

    }

    @Test
    void testServiceReleaseProblem() throws InterruptedException, JsonProcessingException {

        mockWebServer.enqueue(new MockResponse().setBody("{\n"
            + "    \"status\": 204\n"
            + "}\n")
            .addHeader("Content-Type", "application/json"));

        Integer response = judgeService.releaseProblem(135363333330042880L);

        assertEquals(204, response);
        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("POST", recordedRequest.getMethod());

        assertEquals(RELEASE_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode request = objectMapper.readTree(recordedRequest.getBody().readUtf8());

        assertEquals(135363333330042880L, request.get(USER_ID).asLong());

    }

    @Test
    void testServiceAnswerProblem() throws InterruptedException, JsonProcessingException {

        mockWebServer.enqueue(new MockResponse().setBody("{\n"
            + "    \"timeRemaining\": 295.0,\n"
            + "    \"verdict\": \"ac\",\n"
            + "    \"score\": 98.66666666666667,\n"
            + "    \"status\": 200\n"
            + "}\n")
            .addHeader("Content-Type", "application/json"));

        JsonNode response = judgeService.answerProblem(135363333330042880L, "lima");

        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("POST", recordedRequest.getMethod());

        assertEquals(ANSWER_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode request = objectMapper.readTree(recordedRequest.getBody().readUtf8());

        assertEquals(135363333330042880L, request.get(USER_ID).asLong());

        assertEquals("ac", response.get("verdict").asText());

    }

    @Test
    void testServiceCheckUser() throws InterruptedException, JsonProcessingException {

        mockWebServer.enqueue(new MockResponse().setBody("{\n"
            + "    \"userId\": 6969420,\n"
            + "    \"username\": \"hocky\",\n"
            + "    \"score\": 0.0,\n"
            + "    \"problemsSolved\": [],\n"
            + "    \"currentSolve\": null,\n"
            + "    \"status\": 200\n"
            + "}\n")
            .addHeader("Content-Type", "application/json"));

        JsonNode response = judgeService.checkUser(135363333330042880L);

        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("GET", recordedRequest.getMethod());

        assertEquals(USER_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());


        assertEquals("id=135363333330042880",
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getQuery());

        assertEquals("hocky", response.get(USERNAME).asText());

    }

    @Test
    void testServiceInitUser() throws InterruptedException, JsonProcessingException {

        mockWebServer.enqueue(new MockResponse().setBody("{\n"
            + "    \"userId\": 6969420,\n"
            + "    \"username\": \"hocky\",\n"
            + "    \"score\": 0.0,\n"
            + "    \"problemsSolved\": [],\n"
            + "    \"currentSolve\": null,\n"
            + "    \"status\": 200\n"
            + "}\n")
            .addHeader("Content-Type", "application/json"));

        JsonNode response = judgeService.initUser(135363333330042880L, "hocky");

        RecordedRequest recordedRequest = mockWebServer.takeRequest();
        assertEquals("POST", recordedRequest.getMethod());

        assertEquals(INIT_URL,
            Objects.requireNonNull(recordedRequest.getRequestUrl()).uri().getRawPath());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode request = objectMapper.readTree(recordedRequest.getBody().readUtf8());

        assertEquals(135363333330042880L, request.get(USER_ID).asLong());
        assertEquals("hocky", request.get(USERNAME).asText());

        assertEquals("hocky", response.get(USERNAME).asText());

    }

}
