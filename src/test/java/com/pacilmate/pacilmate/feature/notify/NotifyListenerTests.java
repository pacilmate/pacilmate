package com.pacilmate.pacilmate.feature.notify;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class NotifyListenerTests {

    @Mock
    private NotifierMediator notifierMediator;

    @Mock
    private PacilmateUtility pacilmateUtility;

    @InjectMocks
    private NotifierListener notifierListener;

    @Test
    void testListenerIsResponding() {

        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!remindme, 10");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(notifierMediator.createTimer(any(User.class), anyString()))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        notifierListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(1)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }

    @Test
    void testListenerDoesntRespondGibberish() {

        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!ping, test");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(false)
            .thenReturn(false);

        notifierListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(1)).isQuery(any(), any());
        verify(channel, times(0)).sendMessage(any(Message.class));
    }
}
