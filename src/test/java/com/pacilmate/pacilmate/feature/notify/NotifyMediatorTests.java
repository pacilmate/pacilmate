package com.pacilmate.pacilmate.feature.notify;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class NotifyMediatorTests {
    @Mock
    PacilmateUtility pacilmateUtility;

    @InjectMocks
    NotifierMediatorImpl notifierMediator;

    @Mock
    NotifierService notifierService;

    PacilmateUser pacilmateUser;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Jeongyeon");
        when(mockUser.getDiscriminator()).thenReturn("Jeongyeon#1234");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
    }

    @Test
    void testCreateTimer1Min() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        String command = "!remindme, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        User author = mock(User.class);
        Message returnMessage = notifierMediator.createTimer(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("I'll remind you in 1 minute"));
    }

    @Test
    void testCreateTimer2Min() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("2");
        String command = "!remindme, 2";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        User author = mock(User.class);
        Message returnMessage = notifierMediator.createTimer(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("I'll remind you in 2 minutes"));
    }

    @Test
    void testCreateTimerUnverifiedCommand() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        String command = "!remindme, a";
        arguments.add("a");

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        User author = mock(User.class);
        Message returnMessage = notifierMediator.createTimer(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(
            returnMessage.getContentRaw().contains("Sorry, there was a problem running the command")
        );
    }
}
