package com.pacilmate.pacilmate.feature.notify;

import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class NotifyServiceTests {

    @Spy
    PacilmateUtilityImpl pacilmateUtility;

    @Spy
    @InjectMocks
    NotifierServiceImpl notifierService;

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Mock
    JDA jda;

    PacilmateCalendar pacilmateCalendar = mock(PacilmateCalendar.class);
    PacilmateEvent pacilmateEvent = mock(PacilmateEvent.class);
    User mockUser;
    OffsetDateTime now = OffsetDateTime.now().plusMinutes(30);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Sana");
        when(mockUser.getDiscriminator()).thenReturn("Sana#1111");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        when(pacilmateCalendar.getDescription()).thenReturn("test");
        when(pacilmateCalendar.getCalendarId()).thenReturn(1);

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateCalendar.getEventList()).thenReturn(events);

        when(pacilmateEvent.getTime()).thenReturn(now);
    }

    @Test
    void testCreateTimer() {
        notifierService.createTimer(1L, mockUser);
    }

    @Test
    void testSendReminderDM() {

    }

    @Test
    void testCheckForUpcomingEvent() {
        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);
        when(pacilmateCalendarRepository.findAll()).thenReturn(calendars);
        doNothing().when(notifierService).sendReminderDM(anyList(), anyList());

        notifierService.checkForUpcomingEvent();

        verify(pacilmateCalendarRepository, times(1)).findAll();
        verify(pacilmateCalendar, times(1)).getSubscriber();
        verify(pacilmateCalendar, times(1)).getEventList();
        verify(notifierService).sendReminderDM(anyList(), anyList());
    }
}
