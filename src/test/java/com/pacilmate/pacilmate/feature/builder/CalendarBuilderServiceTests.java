package com.pacilmate.pacilmate.feature.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class CalendarBuilderServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Spy
    PacilmateUtilityImpl pacilmateUtility;

    @InjectMocks
    CalendarBuilderServiceImpl calendarBuilderService;

    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar =
            new PacilmateCalendar("testCalendar", pacilmateUser, "#3e9f85");
    }

    @Test
    void testCalendarService() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);
        when(pacilmateCalendarRepository.save(any())).thenReturn(pacilmateCalendar);

        assertEquals(pacilmateCalendar,
            calendarBuilderService.makeCalendar(123123123L, "testCalendar"));

        verify(pacilmateUserRepository, times(1))
            .findByUserId(anyLong());

        verify(pacilmateCalendarRepository, times(1))
            .save(any(PacilmateCalendar.class));
    }
}
