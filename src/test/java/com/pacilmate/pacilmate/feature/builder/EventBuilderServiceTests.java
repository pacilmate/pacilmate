package com.pacilmate.pacilmate.feature.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class EventBuilderServiceTests {

    @Mock
    PacilmateEventRepository pacilmateEventRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Spy
    PacilmateUtilityImpl pacilmateUtility;

    @InjectMocks
    EventBuilderServiceImpl eventBuilderService;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testEventService() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);
        when(pacilmateEventRepository.save(any())).thenReturn(pacilmateEvent);

        assertEquals(pacilmateEvent,
            eventBuilderService.makeEvent(123123123L, "testCalendar", eventTime));

        verify(pacilmateUserRepository, times(1))
            .findByUserId(anyLong());

        verify(pacilmateEventRepository, times(1))
            .save(any(PacilmateEvent.class));
    }
}
