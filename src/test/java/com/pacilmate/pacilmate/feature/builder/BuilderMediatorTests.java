package com.pacilmate.pacilmate.feature.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BuilderMediatorTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @Spy
    PacilmateCalendarEmbedBuilder pacilmateCalendarEmbedBuilder;

    @Mock
    PacilmateEventEmbedBuilder pacilmateEventEmbedBuilder;

    @Mock
    CalendarBuilderService calendarBuilderService;

    @Mock
    EventBuilderService eventBuilderService;

    @Mock
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @InjectMocks
    BuilderMediatorImpl builderMediator;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testCalendarBuildOK() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("test");
        String command = "!makecal, test";
        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(calendarBuilderService.makeCalendar(anyLong(), anyString()))
            .thenReturn(pacilmateCalendar);

        User author = mock(User.class);
        Message returnMessage = builderMediator.buildCalendar(author, command);

        verify(pacilmateCalendarEmbedBuilder, times(1))
            .makeEmbed(any(PacilmateCalendar.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertEquals("test", returnMessage.getEmbeds().get(0).getTitle());
        assertTrue(returnMessage.getContentRaw().contains("Calendar **test** has been"));

    }


    @Test
    void testEventBuildOK()
        throws PacilmateUnverifiedCommandException, PacilmateParseDateTimeException {
        List<String> arguments = new ArrayList<>();
        arguments.add("test");
        arguments.add("+4h");
        arguments.add("today");
        String command = "!makecal, test, +4h, today";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(pacilmateDateTimeParser.parse(anyString(), anyString())).thenReturn(eventTime);
        when(eventBuilderService.makeEvent(anyLong(), anyString(), any(OffsetDateTime.class)))
            .thenReturn(pacilmateEvent);
        when(pacilmateEventEmbedBuilder.makeEmbed(any(PacilmateEvent.class)))
            .thenReturn(mock(MessageEmbed.class));

        User author = mock(User.class);
        Message returnMessage = builderMediator.buildEvent(author, command);

        verify(pacilmateEventEmbedBuilder, times(1))
            .makeEmbed(any(PacilmateEvent.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("The event **test** has been"));

    }

    @Test
    void testCalendarBuildUnverified() throws PacilmateUnverifiedCommandException {

        String command = "!makecal,";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = builderMediator.buildCalendar(author, command);

        verify(pacilmateCalendarEmbedBuilder, times(0))
            .makeEmbed(any(PacilmateCalendar.class));

        verify(eventBuilderService, times(0))
            .makeEvent(anyLong(), anyString(), any(OffsetDateTime.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testEventBuildUnverified() throws PacilmateUnverifiedCommandException {

        String command = "!makeev, test";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);
        Message returnMessage = builderMediator.buildEvent(author, command);

        verify(pacilmateEventEmbedBuilder, times(0))
            .makeEmbed(any(PacilmateEvent.class));

        verify(calendarBuilderService, times(0))
            .makeCalendar(anyLong(), anyString());

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }
}
