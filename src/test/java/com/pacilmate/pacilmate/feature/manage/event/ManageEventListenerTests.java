package com.pacilmate.pacilmate.feature.manage.event;

import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ManageEventListenerTests {
    @Mock
    private ManageEventMediator manageEventMediator;

    @Mock
    private PacilmateUtility pacilmateUtility;

    @InjectMocks
    private ManageEventListener manageEventListener;

    @Test
    void testListenerIsRespondingSetEvent() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!set, 1, 2");
        when(message.getAuthor()).thenReturn(author);
        when(messageReceivedEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(manageEventMediator.setEvent(any(User.class), anyString()))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        manageEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(1)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }

    @Test
    void testListenerIsRespondingUnsetEvent() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!unset, 1, 2");
        when(message.getAuthor()).thenReturn(author);
        when(messageReceivedEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(false).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(manageEventMediator.unsetEvent(any(User.class), anyString()))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        manageEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(2)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }
}
