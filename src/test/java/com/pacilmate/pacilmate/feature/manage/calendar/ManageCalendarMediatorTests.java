package com.pacilmate.pacilmate.feature.manage.calendar;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.exception.PacilmateUserAlreadySubscribedException;
import com.pacilmate.pacilmate.exception.PacilmateUserNotSubscribedException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ManageCalendarMediatorTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @Mock
    ManageCalendarService manageCalendarService;

    @InjectMocks
    ManageCalendarMediatorImpl manageCalendarMediator;

    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Jihyo");
        when(mockUser.getDiscriminator()).thenReturn("Jihyo#1234");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("dummyCalendar", pacilmateUser, "#ffffff");
    }

    @Test
    void testSubCalendarOK()
        throws PacilmateUnverifiedCommandException, PacilmateUserAlreadySubscribedException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        String command = "!sub, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(manageCalendarService.subCalendar(anyLong(), anyInt())).thenReturn(pacilmateCalendar);

        User author = mock(User.class);
        Message returnMessage = manageCalendarMediator.subCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Successfully **subscribed** to"));
    }

    @Test
    void testUnsubCalendarOK()
        throws PacilmateUserNotSubscribedException, PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        String command = "!unsub, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(manageCalendarService.unsubCalendar(anyLong(), anyInt()))
            .thenReturn(pacilmateCalendar);

        User author = mock(User.class);
        Message returnMessage = manageCalendarMediator.unsubCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Successfully **unsubscribed** to"));
    }

    @Test
    void testSubCalendarUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!sub";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = manageCalendarMediator.subCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testUnsubCalendarUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!unsub";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = manageCalendarMediator.unsubCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testSubCalendarOtherException()
        throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("test");
        String command = "!sub, test";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        User author = mock(User.class);
        Message returnMessage = manageCalendarMediator.subCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("Sorry, there was a problem running the command"));
    }

    @Test
    void testUnsubCalendarOtherException()
        throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("test");
        String command = "!unsub, test";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        User author = mock(User.class);
        Message returnMessage = manageCalendarMediator.unsubCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("Sorry, there was a problem running the command"));
    }
}
