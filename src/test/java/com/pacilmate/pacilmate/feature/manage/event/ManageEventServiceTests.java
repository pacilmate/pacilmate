package com.pacilmate.pacilmate.feature.manage.event;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class ManageEventServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateEventRepository pacilmateEventRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Mock
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;

    @InjectMocks
    ManageEventServiceImpl manageEventService;
    OffsetDateTime eventTime;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    PacilmateUser pacilmateUser;
    User mockUser = mock(User.class);

    @BeforeEach
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(6969L);
        when(mockUser.getName()).thenReturn("JC Denton");
        when(mockUser.getDiscriminator()).thenReturn("JC Denton#6969");
        when(mockUser.getEffectiveAvatarUrl())
            .thenReturn("https://wallpapercave.com/wp/wp4406684.jpg");

        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#ffffff");
        pacilmateCalendar.setCalendarId(1);
        pacilmateEvent = new PacilmateEvent("testev", eventTime, pacilmateUser, "#ffffff");
        pacilmateEvent.setEventId(2);
    }

    @Test
    void testSetEventServiceOK() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.setEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw().contains("Successfully set"));
    }

    @Test
    void testSetEventNullCalendar() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(null);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.setEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw()
                .contains("Whoops! Sorry, this calendar does not exist!"));
    }

    @Test
    void testSetEventNullEvent() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(null);

        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.setEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw().contains("Whoops! Sorry, this event does not exist!"));
    }

    @Test
    void testSetEventDifferentAuthorId() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        pacilmateCalendar.getAuthor().setUserId(420L);
        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.setEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw()
                .contains("Whoops! Sorry, this calendar does not belong to you."));
    }

    @Test
    void testUnsetEventServiceOK() {
        ArrayList<PacilmateEvent> eventList = new ArrayList<>();
        eventList.add(pacilmateEvent);
        pacilmateCalendar.setEventList(eventList);
        ArrayList<PacilmateCalendar> calendarList = new ArrayList<>();
        calendarList.add(pacilmateCalendar);
        pacilmateEvent.setCalendarList(calendarList);
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        MessageBuilder messageBuilder = manageEventService.unsetEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw().contains("Successfully unset"));
    }

    @Test
    void testUnsetEventNullCalendar() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(null);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.unsetEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw()
                .contains("Whoops! Sorry, this calendar does not exist!"));
    }

    @Test
    void testUnsetEventNullEvent() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(null);

        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.unsetEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw().contains("Whoops! Sorry, this event does not exist!"));
    }

    @Test
    void testUnsetEventDifferentAuthorId() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateEventRepository.findByEventId(anyInt())).thenReturn(pacilmateEvent);

        pacilmateCalendar.getAuthor().setUserId(420L);
        pacilmateCalendar.setEventList(new ArrayList<>());
        pacilmateEvent.setCalendarList(new ArrayList<>());

        MessageBuilder messageBuilder = manageEventService.unsetEvent(6969L, 1, 2);
        Message message = messageBuilder.build();
        assertTrue(message.getContentRaw()
                .contains("Whoops! Sorry, this calendar does not belong to you."));
    }
}