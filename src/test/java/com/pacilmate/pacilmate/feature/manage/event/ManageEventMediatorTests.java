package com.pacilmate.pacilmate.feature.manage.event;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ManageEventMediatorTests {
    @InjectMocks
    ManageEventMediatorImpl eventMediator;

    @Mock
    ManageEventServiceImpl manageEventService;

    @Mock
    MessageBuilder messageBuilder;

    @Mock
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @Spy
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;

    @Mock
    PacilmateUtility pacilmateUtility;

    OffsetDateTime eventTime;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    PacilmateUser pacilmateUser;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(1412L);
        when(mockUser.getName()).thenReturn("JC Denton");
        when(mockUser.getDiscriminator()).thenReturn("JC Denton#1412");
        when(mockUser.getEffectiveAvatarUrl())
            .thenReturn("https://wallpapercave.com/wp/wp4406684.jpg");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now().plusHours(4);
        pacilmateEvent = new PacilmateEvent("testev", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testSetEventOK() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("2");
        String command = "!set, 1, 2";
        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(manageEventService.setEvent(anyLong(), anyInt(), anyInt())).thenReturn(messageBuilder);

        User author = mock(User.class);
        Message returnMessage = eventMediator.setEvent(author, command);

        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
    }

    @Test
    void testUnsetEventOK() throws PacilmateUnverifiedCommandException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("2");
        String command = "!unset, 1, 2";
        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);
        when(manageEventService.unsetEvent(anyLong(), anyInt(), anyInt()))
            .thenReturn(messageBuilder);

        User author = mock(User.class);
        Message returnMessage = eventMediator.unsetEvent(author, command);

        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
    }

    @Test
    void testUnverifiedArgumentsUnSetEvent() throws PacilmateUnverifiedCommandException {

        String command = "!unset, 1, 2";
        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Yada yada"));

        User author = mock(User.class);
        eventMediator.unsetEvent(author, command);

        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
        verifyNoInteractions(manageEventService);
    }

    @Test
    void testUnverifiedArgumentsSetEvent() throws PacilmateUnverifiedCommandException {

        String command = "!set, 1, 2";
        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Yada yada"));

        User author = mock(User.class);
        eventMediator.setEvent(author, command);

        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
        verifyNoInteractions(manageEventService);
    }
}
