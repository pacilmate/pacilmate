package com.pacilmate.pacilmate.feature.manage.calendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.pacilmate.pacilmate.exception.PacilmateUserAlreadySubscribedException;
import com.pacilmate.pacilmate.exception.PacilmateUserNotSubscribedException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class ManageCalendarServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Spy
    PacilmateUtilityImpl pacilmateUtility;

    @InjectMocks
    ManageCalendarServiceImpl manageCalendarService;

    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar = mock(PacilmateCalendar.class);
    User mockUser = mock(User.class);


    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Sana");
        when(mockUser.getDiscriminator()).thenReturn("Sana#1111");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);

        when(pacilmateCalendar.getDescription()).thenReturn("test");
        when(pacilmateCalendar.getCalendarId()).thenReturn(1);
    }

    @Test
    void testSubCalendarServiceOK() throws PacilmateUserAlreadySubscribedException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateCalendarRepository.save(any())).thenReturn(pacilmateCalendar);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateUser> subscribers = new ArrayList<>();
        when(pacilmateCalendar.getSubscriber()).thenReturn(subscribers);

        assertEquals(pacilmateCalendar, manageCalendarService.subCalendar(123123123L, 1));
    }

    @Test
    void testUnsubCalendarServiceOK() throws PacilmateUserNotSubscribedException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateCalendarRepository.save(any())).thenReturn(pacilmateCalendar);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateUser> subscribers = new ArrayList<>();
        subscribers.add(pacilmateUser);
        when(pacilmateCalendar.getSubscriber()).thenReturn(subscribers);

        assertEquals(pacilmateCalendar, manageCalendarService.unsubCalendar(123123123L, 1));
    }

    @Test
    void testSubCalendarServiceAlreadySubscribed() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateUser> subscribers = new ArrayList<>();
        subscribers.add(pacilmateUser);
        when(pacilmateCalendar.getSubscriber()).thenReturn(subscribers);

        PacilmateUserAlreadySubscribedException exception =
            assertThrows(
                PacilmateUserAlreadySubscribedException.class,
                () -> manageCalendarService.subCalendar(123123123L, 1)
            );
        assertTrue(exception.getMessage().contains("You are already subscribed to this calendar"));
    }

    @Test
    void testUnsubCalendarNotSubscribedYet() {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateUser> subscribers = new ArrayList<>();
        when(pacilmateCalendar.getSubscriber()).thenReturn(subscribers);

        PacilmateUserNotSubscribedException exception =
            assertThrows(
                PacilmateUserNotSubscribedException.class,
                () -> manageCalendarService.unsubCalendar(123123123L, 1)
            );
        assertTrue(exception.getMessage().contains("You are not subscribed to this calendar yet"));
    }
}
