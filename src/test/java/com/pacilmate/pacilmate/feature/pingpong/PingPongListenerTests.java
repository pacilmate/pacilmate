package com.pacilmate.pacilmate.feature.pingpong;

import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PingPongListenerTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @InjectMocks
    private PingPongListener pingPongListener;

    @Test
    void testListenerIsRespondingPing() {

        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(true);

        when(messageEvent.getMessage()).thenReturn(message);

        when(message.getContentRaw()).thenReturn("!ping");

        MessageChannel channel = mock(MessageChannel.class);
        when(messageEvent.getChannel()).thenReturn(channel);

        MessageAction afterSend = mock(MessageAction.class);

        when(channel.sendMessage(anyString())).thenReturn(afterSend);

        doNothing().when(afterSend).queue(any());

        pingPongListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(messageEvent, times(1)).getChannel();
        verify(channel, times(1)).sendMessage(anyString());
        verify(afterSend, times(1)).queue(any());
    }


    @Test
    void testPongAcceptedGoodPing() {

        Message message = mock(ReceivedMessage.class);
        MessageAction editPong = mock(MessageAction.class);

        when(message.getTimeCreated()).thenReturn(OffsetDateTime.now(ZoneId.of("+7")));
        when(message.editMessageFormat(anyString(), any())).thenReturn(editPong);
        doNothing().when(editPong).queue();

        pingPongListener.pongAccepted(message);

        verify(message, times(1)).getTimeCreated();
        verify(message, times(1)).editMessageFormat(anyString(), any());
        verify(editPong, times(1)).queue();
    }

    @Test
    void testListenerIsIgnoringNonPing() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);

        when(messageEvent.getMessage()).thenReturn(message);

        when(message.getContentRaw()).thenReturn("bukanping");

        pingPongListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(messageEvent, times(0)).getChannel();

    }
}
