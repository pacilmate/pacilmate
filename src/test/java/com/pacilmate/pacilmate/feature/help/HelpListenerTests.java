package com.pacilmate.pacilmate.feature.help;

import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HelpListenerTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @InjectMocks
    private HelpListener helpListener;

    @Test
    void testListenerIsRespondingHelp() {

        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(true);

        when(messageEvent.getMessage()).thenReturn(message);

        when(message.getContentRaw()).thenReturn("!help");

        MessageChannel channel = mock(MessageChannel.class);
        when(messageEvent.getChannel()).thenReturn(channel);

        MessageAction afterSend = mock(MessageAction.class);

        when(channel.sendMessage((MessageEmbed) any())).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        helpListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(messageEvent, times(1)).getChannel();
        verify(channel, times(1)).sendMessage((MessageEmbed) any());
        verify(afterSend, times(1)).queue();
    }

    @Test
    void testListenerIsIgnoringNonHelp() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);

        when(messageEvent.getMessage()).thenReturn(message);

        when(message.getContentRaw()).thenReturn("bukanhelp");

        helpListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(messageEvent, times(0)).getChannel();

    }

}
