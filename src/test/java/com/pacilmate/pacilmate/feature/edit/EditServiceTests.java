package com.pacilmate.pacilmate.feature.edit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnauthorizedEditException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateEventRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class EditServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateEventRepository pacilmateEventRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @Spy
    PacilmateUtilityImpl pacilmateUtility;

    @InjectMocks
    EditServiceImpl editService;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateUser pacilmateAnotherUser;
    PacilmateEvent pacilmateEvent;
    PacilmateCalendar pacilmateCalendar;
    User mockUser;
    User mockUserAnother;

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        mockUserAnother = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        when(mockUserAnother.getIdLong()).thenReturn(6969420L);
        when(mockUserAnother.getName()).thenReturn("Akira hu ha");
        when(mockUserAnother.getDiscriminator()).thenReturn("akira#0000");
        when(mockUserAnother.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateAnotherUser = new PacilmateUser(mockUserAnother);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testEditCalendarSucceed() throws PacilmateUnauthorizedEditException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt()))
            .thenReturn(pacilmateCalendar);

        when(pacilmateCalendarRepository.save(any(PacilmateCalendar.class)))
            .thenReturn(pacilmateCalendar);

        editService.editCalendar(123123123L, 2, "Coba Ganti");


        assertEquals("Coba Ganti", pacilmateCalendar.getDescription());
        verify(pacilmateCalendarRepository, times(1))
            .save(any(PacilmateCalendar.class));
        verify(pacilmateCalendarRepository, times(1))
            .findByCalendarId(anyInt());
    }

    @Test
    void testEditCalendarFailedNull() throws PacilmateUnauthorizedEditException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt()))
            .thenReturn(null);

        assertThrows(PacilmateUnauthorizedEditException.class, () -> {
            editService.editCalendar(123123123L, 2, "Coba Ganti");
        });


        assertEquals("test", pacilmateCalendar.getDescription());
        verify(pacilmateCalendarRepository, times(0))
            .save(any(PacilmateCalendar.class));
        verify(pacilmateCalendarRepository, times(1))
            .findByCalendarId(anyInt());
    }

    @Test
    void testEditEventSucceed() throws PacilmateUnauthorizedEditException {

        when(pacilmateEventRepository.findByEventId(anyInt()))
            .thenReturn(pacilmateEvent);

        when(pacilmateEventRepository.save(any(PacilmateEvent.class)))
            .thenReturn(pacilmateEvent);

        editService.editEvent(123123123L, 2, "Coba Ganti", eventTime);

        assertEquals("Coba Ganti", pacilmateEvent.getDescription());
        verify(pacilmateEventRepository, times(1))
            .save(any(PacilmateEvent.class));
        verify(pacilmateEventRepository, times(1))
            .findByEventId(anyInt());
    }

    @Test
    void testDeleteCalendarSucceed() throws PacilmateUnauthorizedEditException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt()))
            .thenReturn(pacilmateCalendar);

        doNothing().when(pacilmateCalendarRepository).delete(any(PacilmateCalendar.class));

        editService.deleteCalendar(123123123L, 2);

        verify(pacilmateCalendarRepository, times(1))
            .delete(any(PacilmateCalendar.class));
        verify(pacilmateCalendarRepository, times(1))
            .findByCalendarId(anyInt());
    }

    @Test
    void testDeleteCalendarUnverified() throws PacilmateUnauthorizedEditException {
        when(pacilmateCalendarRepository.findByCalendarId(anyInt()))
            .thenReturn(pacilmateCalendar);

        assertThrows(PacilmateUnauthorizedEditException.class, () -> {
            editService.deleteCalendar(69420420L, 2);
        });

        verify(pacilmateCalendarRepository, times(0))
            .delete(any(PacilmateCalendar.class));
        verify(pacilmateCalendarRepository, times(1))
            .findByCalendarId(anyInt());
    }

    @Test
    void testDeleteEventSucceed() throws PacilmateUnauthorizedEditException {
        when(pacilmateEventRepository.findByEventId(anyInt()))
            .thenReturn(pacilmateEvent);

        doNothing().when(pacilmateEventRepository).delete(any(PacilmateEvent.class));

        editService.deleteEvent(123123123L, 2);

        verify(pacilmateEventRepository, times(1))
            .delete(any(PacilmateEvent.class));
        verify(pacilmateEventRepository, times(1))
            .findByEventId(anyInt());
    }

    @Test
    void testDeleteEventUnverified() throws PacilmateUnauthorizedEditException {
        when(pacilmateEventRepository.findByEventId(anyInt()))
            .thenReturn(pacilmateEvent);

        assertThrows(PacilmateUnauthorizedEditException.class, () -> {
            editService.deleteEvent(69420420L, 2);
        });
        assertNotEquals(69420420L, pacilmateEvent.getAuthor().getUserId());
        verify(pacilmateEventRepository, times(0))
            .delete(any(PacilmateEvent.class));
        verify(pacilmateEventRepository, times(1))
            .findByEventId(anyInt());
    }

    @Test
    void testDeleteEventNull() throws PacilmateUnauthorizedEditException {
        when(pacilmateEventRepository.findByEventId(anyInt()))
            .thenReturn(null);

        assertThrows(PacilmateUnauthorizedEditException.class, () -> {
            editService.deleteEvent(69420420L, 2);
        });
        verify(pacilmateEventRepository, times(0))
            .delete(any(PacilmateEvent.class));
        verify(pacilmateEventRepository, times(1))
            .findByEventId(anyInt());
    }
}
