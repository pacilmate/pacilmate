package com.pacilmate.pacilmate.feature.edit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.exception.PacilmateUnauthorizedEditException;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class EditMediatorTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @Mock
    EditService editService;

    @Mock
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @Spy
    PacilmateCalendarEmbedBuilder pacilmateCalendarEmbedBuilder;

    @Mock
    PacilmateEventEmbedBuilder pacilmateEventEmbedBuilder;

    @InjectMocks
    EditMediatorImpl editMediator;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }

    @Test
    void testEditCalendarSucceed()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {

        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("test");
        String command = "!editcal, 1, test";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);

        when(editService.editCalendar(anyLong(), anyInt(), anyString()))
            .thenReturn(pacilmateCalendar);

        User author = mock(User.class);
        Message returnMessage = editMediator.editCalendar(author, command);

        verify(pacilmateCalendarEmbedBuilder, times(1))
            .makeEmbed(any(PacilmateCalendar.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertEquals("test", returnMessage.getEmbeds().get(0).getTitle());
        assertTrue(returnMessage.getContentRaw().contains("Calendar **test** has been"));

    }

    @Test
    void testEditCalendarUnverifiedCommand()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {

        String command = "!editcal, 1, test";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Coba"));

        User author = mock(User.class);
        Message returnMessage = editMediator.editCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Coba"));

    }

    @Test
    void testEditEventOK()
        throws PacilmateUnauthorizedEditException, PacilmateUnverifiedCommandException,
        PacilmateParseDateTimeException {

        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("test");
        arguments.add("+4h");
        arguments.add("today");
        String command = "!editev, 1, test, +4h, today";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);

        when(pacilmateDateTimeParser.parse(anyString(), anyString()))
            .thenReturn(eventTime);

        when(editService.editEvent(anyLong(), anyInt(), anyString(), any(OffsetDateTime.class)))
            .thenReturn(pacilmateEvent);

        when(pacilmateEventEmbedBuilder.makeEmbed(any(PacilmateEvent.class)))
            .thenReturn(mock(MessageEmbed.class));

        User author = mock(User.class);
        Message returnMessage = editMediator.editEvent(author, command);

        verify(pacilmateEventEmbedBuilder, times(1))
            .makeEmbed(any(PacilmateEvent.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("The event **test** has been"));

    }

    @Test
    void testEditEventException()
        throws PacilmateUnauthorizedEditException, PacilmateUnverifiedCommandException,
        PacilmateParseDateTimeException {

        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("test");
        arguments.add("+4h");
        arguments.add("today");
        String command = "!editev, 1, test, +4h, today";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);

        when(pacilmateDateTimeParser.parse(anyString(), anyString()))
            .thenReturn(eventTime);

        when(editService.editEvent(anyLong(), anyInt(), anyString(), any(OffsetDateTime.class)))
            .thenThrow(new PacilmateUnauthorizedEditException("Coba"));

        User author = mock(User.class);
        Message returnMessage = editMediator.editEvent(author, command);

        verify(pacilmateEventEmbedBuilder, times(0))
            .makeEmbed(any(PacilmateEvent.class));

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Coba"));

    }

    @Test
    void testDeleteCalendarOK()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");

        String command = "!delcal, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);

        doNothing().when(editService).deleteCalendar(anyLong(), anyInt());

        User author = mock(User.class);
        Message returnMessage = editMediator.deleteCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Calendar has been deleted!"));
    }

    @Test
    void testDeleteEventOK()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");

        String command = "!delev, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenReturn(arguments);

        doNothing().when(editService).deleteEvent(anyLong(), anyInt());

        User author = mock(User.class);
        Message returnMessage = editMediator.deleteEvent(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Event has been deleted!"));
    }

    @Test
    void testDeleteCalendarFailed()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");

        String command = "!delcal, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Coba"));

        User author = mock(User.class);
        Message returnMessage = editMediator.deleteCalendar(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Coba"));
    }

    @Test
    void testDeleteEventFailed()
        throws PacilmateUnverifiedCommandException, PacilmateUnauthorizedEditException {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");

        String command = "!delev, 1";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Coba lagi"));

        User author = mock(User.class);
        Message returnMessage = editMediator.deleteEvent(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Coba lagi"));
    }
}
