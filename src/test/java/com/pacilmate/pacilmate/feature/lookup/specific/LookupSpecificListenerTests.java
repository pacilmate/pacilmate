package com.pacilmate.pacilmate.feature.lookup.specific;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtilityImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LookupSpecificListenerTests {

    @Spy
    private PacilmateUtilityImpl pacilmateUtility;
    @Mock
    private LookupSpecificMediator lookupSpecificMediator;
    @Mock
    private MessageReceivedEvent messageReceivedEvent;
    @Mock
    private Message message;
    @Mock
    private User author;
    @Mock
    private MessageChannel channel;
    @Mock
    private MessageAction afterSend;
    @Mock
    private Message responseMessage;

    @InjectMocks
    LookupSpecificListener lookupSpecificListener;

    @BeforeEach
    public void setup() {
        lenient().when(messageReceivedEvent.getMessage()).thenReturn(message);
        lenient().when(message.getAuthor()).thenReturn(author);
        lenient().when(messageReceivedEvent.getChannel()).thenReturn(channel);
        lenient().when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);
    }

    @ParameterizedTest
    @ValueSource(strings = {"cal"})
    void testValidQueriesOk(String query) {
        query = "!" + query;
        when(message.getContentRaw()).thenReturn(query);

        when(lookupSpecificMediator.lookupCalendar(any(User.class), anyString()))
            .thenReturn(responseMessage);
        doNothing().when(afterSend).queue();

        lookupSpecificListener.onMessageReceived(messageReceivedEvent);


        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(messageReceivedEvent, times(1)).getMessage();

        verify(pacilmateUtility, atLeastOnce()).isQuery(anyString(), anyString());
    }

    @Test
    void testGiberrishOk() {
        String query = "!sasdasd";
        when(message.getContentRaw()).thenReturn(query);

        lookupSpecificListener.onMessageReceived(messageReceivedEvent);
        verify(message, times(1)).getAuthor();
        verify(messageReceivedEvent, times(1)).getChannel();
        verify(channel, times(0)).sendMessage(anyString());
        verify(messageReceivedEvent, times(1)).getMessage();
    }
}
