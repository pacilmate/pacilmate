package com.pacilmate.pacilmate.feature.lookup.global;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateCalendarMessageBuilder;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LookupGlobalMediatorTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @InjectMocks
    LookupGlobalMediatorImpl lookupGlobalMediator;

    @Mock
    LookupCalendarService lookupCalendarService;

    @Mock
    LookupEventService lookupEventService;

    @Mock
    PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @Mock
    PacilmateCalendarMessageBuilder pacilmateCalendarMessageBuilder;

    @Mock
    PacilmateGlobalCalendarEmbedBuilder globalCalendarEmbedBuilder;

    @Mock
    PacilmateGlobalEventEmbedBuilder globalEventEmbedBuilder;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("rigel");
        when(mockUser.getDiscriminator()).thenReturn("betaorionis#0860");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }


    @Test
    void testGetListCalendarUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!listcal";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = lookupGlobalMediator.getListCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testGetListMyCalendarUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!mycal";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = lookupGlobalMediator.getListMyCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testGetListMyEventUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!myev";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = lookupGlobalMediator.getListMyEvent(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testGetListEventUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!tmrw";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = lookupGlobalMediator.getListEvent(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testGetListCalendarEmptyListOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateCalendar> calendars = new ArrayList<>();

        List<String> arguments = new ArrayList<>();
        String command = "!listcal";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupCalendarService.getListSubscribedCalendar(anyLong())).thenReturn(calendars);

        Message returnMessage = lookupGlobalMediator.getListCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("You haven't subscribed to any calendars."));

    }

    @Test
    void testGetListMyCalendarEmptyListOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateCalendar> calendars = new ArrayList<>();

        List<String> arguments = new ArrayList<>();
        String command = "!mycal";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupCalendarService.getListMyCalendar(anyLong())).thenReturn(calendars);

        Message returnMessage = lookupGlobalMediator.getListMyCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("You haven't made any calendars."));

    }

    @Test
    void testGetListMyEventEmptyListOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateEvent> events = new ArrayList<>();

        List<String> arguments = new ArrayList<>();
        String command = "!myev";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupEventService.getMyEvent(anyLong())).thenReturn(events);

        Message returnMessage = lookupGlobalMediator.getListMyEvent(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("You haven't made any events."));

    }

    @Test
    void testGetListEventEmptyListOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateEvent> events = new ArrayList<>();

        List<String> arguments = new ArrayList<>();
        String command = "!tmrw";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupEventService.getSpecifiedEvent(
            any(OffsetDateTime.class), anyLong())
        ).thenReturn(events);

        Message returnMessage = lookupGlobalMediator.getListEvent("tomorrow", author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());
        System.out.println(returnMessage.getContentRaw());

        assertTrue(returnMessage.getContentRaw()
            .contains("You don't have any events for tomorrow."));

    }


    
    @Test
    void testGetListCalendarMediatorOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);

        List<String> arguments = new ArrayList<>();
        String command = "!listcal";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupCalendarService.getListSubscribedCalendar(anyLong())).thenReturn(calendars);

        Message returnMessage = lookupGlobalMediator.getListCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("Here's list all of your subscribed calendar(s)."));

    }

    @Test
    void testGetListMyCalendarMediatorOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);

        List<String> arguments = new ArrayList<>();
        String command = "!mycal";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupCalendarService.getListMyCalendar(anyLong())).thenReturn(calendars);

        Message returnMessage = lookupGlobalMediator.getListMyCalendar(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("Here's list all calendar(s) made by you."));

    }

    @Test
    void testGetListMyEventMediatorOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);

        List<String> arguments = new ArrayList<>();
        String command = "!myev";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupEventService.getMyEvent(anyLong())).thenReturn(events);

        Message returnMessage = lookupGlobalMediator.getListMyEvent(command, author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Here's list all event(s) made by you."));

    }

    @Test
    void testGetListEventMediatorOK() throws PacilmateUnverifiedCommandException {

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);

        List<String> arguments = new ArrayList<>();
        String command = "!tmrw";
        User author = mock(User.class);

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);

        when(lookupEventService.getSpecifiedEvent(
            any(OffsetDateTime.class), anyLong())
        ).thenReturn(events);

        Message returnMessage = lookupGlobalMediator.getListEvent("tomorrow", author);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw()
            .contains("Here's list all event(s) for tomorrow."));

    }

}