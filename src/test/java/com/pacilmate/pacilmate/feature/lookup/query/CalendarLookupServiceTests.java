package com.pacilmate.pacilmate.feature.lookup.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class CalendarLookupServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @InjectMocks
    CalendarLookupServiceImpl calendarLookupService;

    PacilmateUser pacilmateUser = mock(PacilmateUser.class);
    PacilmateCalendar pacilmateCalendar = mock(PacilmateCalendar.class);
    PacilmateEvent pacilmateEvent = mock(PacilmateEvent.class);


    @BeforeAll
    void setupAll() {
        when(pacilmateEvent.getEventId()).thenReturn(1);
        when(pacilmateEvent.getTime()).thenReturn(OffsetDateTime.now().plusDays(1));

        when(pacilmateCalendar.getDescription()).thenReturn("test");
        when(pacilmateCalendar.getCalendarId()).thenReturn(1);
    }

    @Test
    void testListSpecifiedServiceOK() {
        lenient().when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateUser.getMadeEvent()).thenReturn(events);

        assertNotEquals(events, calendarLookupService.getListSpecifiedEvent(123123123L, "2"));
    }

    @Test
    void testFirstEventEventServiceOK() {
        lenient().when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateUser.getMadeEvent()).thenReturn(events);

        OffsetDateTime date = OffsetDateTime.now();

        assertEquals(events, calendarLookupService.getFirstEvent(date, 123123123L, "1"));
    }

}
