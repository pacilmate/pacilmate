package com.pacilmate.pacilmate.feature.lookup.specific;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LookupSpecificMediatorTests {
    @InjectMocks
    LookupSpecificMediatorImpl lookupSpecificMediator;
    @Mock
    PacilmateUtility pacilmateUtility;
    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;
    @Mock
    PacilmateCalendar pacilmateCalendar;
    @Mock
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;
    @Mock
    MessageEmbed messageEmbed;
    @Mock
    private User user;

    @BeforeEach
    void setup() {
        lenient().when(user.getIdLong()).thenReturn(6969420L);
        lenient().when(user.getName()).thenReturn("hocky");
    }

    @Test
    void lookupSpecificMediatorIsOk() throws PacilmateUnverifiedCommandException {

        List<String> arguments = new ArrayList<>();
        arguments.add("12");

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(pacilmateCalendarRepository.findByCalendarId(anyInt())).thenReturn(pacilmateCalendar);
        when(pacilmateManageEventEmbedBuilder.makeEmbed(pacilmateCalendar))
            .thenReturn(messageEmbed);
        lookupSpecificMediator.lookupCalendar(user, "!cal, 12");
        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
    }

    @Test
    void lookupSpecificMediatorIsOkWhenNotVerified() throws PacilmateUnverifiedCommandException {


        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Yada yada"));
        lookupSpecificMediator.lookupCalendar(user, "!cal, 12");
        verify(pacilmateUtility, times(1)).verify(any(User.class), anyString(), anyInt());
        verifyNoInteractions(pacilmateCalendarRepository);
    }
}
