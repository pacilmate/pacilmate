package com.pacilmate.pacilmate.feature.lookup.global;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateCalendarRepository;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class LookupEventServiceTests {

    @Mock
    PacilmateCalendarRepository pacilmateCalendarRepository;

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @InjectMocks
    LookupEventServiceImpl lookupEventService;

    PacilmateUser pacilmateUser = mock(PacilmateUser.class);
    PacilmateCalendar pacilmateCalendar = mock(PacilmateCalendar.class);
    PacilmateEvent pacilmateEvent = mock(PacilmateEvent.class);


    @BeforeAll
    void setupAll() {
        when(pacilmateEvent.getEventId()).thenReturn(1);
        when(pacilmateEvent.getTime()).thenReturn(OffsetDateTime.now().plusDays(1));

        when(pacilmateCalendar.getDescription()).thenReturn("test");
        when(pacilmateCalendar.getCalendarId()).thenReturn(1);
    }

    @Test
    void testGetMyEventServiceOK() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateUser.getMadeEvent()).thenReturn(events);

        assertEquals(events, lookupEventService.getMyEvent(123123123L));
    }

    @Test
    void testGetSpecifiedEventServiceOK() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);
        when(pacilmateUser.getSubscribedCalendar()).thenReturn(calendars);

        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateCalendar.getEventList()).thenReturn(events);

        OffsetDateTime date = OffsetDateTime.now().plusDays(1);

        assertEquals(events, lookupEventService.getSpecifiedEvent(date, 123123123L));
    }
}