package com.pacilmate.pacilmate.feature.lookup;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.feature.lookup.query.CalendarLookupMediatorImpl;
import com.pacilmate.pacilmate.feature.lookup.query.CalendarLookupService;
import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.PacilmateUtility;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeParser;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CalendarLookupMediatorTests {

    @Mock
    PacilmateUtility pacilmateUtility;

    @Mock
    MessageBuilder messageBuilder;

    @Mock
    PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @Mock
    PacilmateDateTimeParser pacilmateDateTimeParser;

    @InjectMocks
    CalendarLookupMediatorImpl calendarLookupMediator;

    @Mock
    CalendarLookupService calendarLookupService;

    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    PacilmateCalendar pacilmateCalendar;
    PacilmateEvent pacilmateEvent;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);
        pacilmateCalendar = new PacilmateCalendar("test", pacilmateUser, "#3e9f85");
        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);
        pacilmateEvent = new PacilmateEvent("test", eventTime, pacilmateUser, "#3e9f85");
    }


    @Test
    void testGetEventListUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!ev";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = calendarLookupMediator.listFirstEvent(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));

    }

    @Test
    void testGetEventDateListUnverified() throws PacilmateUnverifiedCommandException {
        String command = "!evdate";

        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt()))
            .thenThrow(new PacilmateUnverifiedCommandException("Arguments are not enough"));

        User author = mock(User.class);

        Message returnMessage = calendarLookupMediator.listSpecifiedEvent(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Arguments are not enough"));
    }


    @Test
    void testCalendarLookupMediatorOK()
        throws PacilmateUnverifiedCommandException {
        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        String command = "!ev, 1";
        User author = mock(User.class);


        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(calendarLookupService.getFirstEvent(any(OffsetDateTime.class),
            anyLong(), anyString())
        ).thenReturn(events);

        Message returnMessage = calendarLookupMediator.listFirstEvent(author, command);


        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertFalse(returnMessage.getContentRaw().contains("Here's your next 1 event(s)"));

    }

    @Test
    void testCalendarDateLookupMediatorOK()
        throws PacilmateUnverifiedCommandException {
        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);

        List<String> arguments = new ArrayList<>();
        arguments.add("01");
        String command = "!evdate, 01";
        User author = mock(User.class);


        when(pacilmateUtility.verify(any(User.class), anyString(), anyInt())).thenReturn(arguments);
        when(calendarLookupService.getListSpecifiedEvent(
            anyLong(), anyString())
        ).thenReturn(events);

        Message returnMessage = calendarLookupMediator.listSpecifiedEvent(author, command);

        verify(pacilmateUtility, times(1))
            .verify(any(User.class), anyString(), anyInt());

        assertTrue(returnMessage.getContentRaw().contains("Here's your event for 01"));

    }
}
