package com.pacilmate.pacilmate.feature.lookup.global;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.utility.PacilmateUtility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class LookupGlobalListenerTests {

    @Mock
    private LookupGlobalMediator lookupGlobalMediator;

    @Mock
    private PacilmateUtility pacilmateUtility;

    @InjectMocks
    private LookupGlobalListener lookupGlobalListener;

    @Test
    void testListenerIsRespondingToListcal() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!listcal");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString())).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(lookupGlobalMediator.getListCalendar(anyString(), any(User.class)))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        lookupGlobalListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(1)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }


    @Test
    void testListenerIsRespondingToMycal() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!mycal");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString()))
            .thenReturn(false).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(lookupGlobalMediator.getListMyCalendar(anyString(), any(User.class)))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        lookupGlobalListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(2)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }


    @Test
    void testListenerIsRespondingToMyev() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!myev");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString()))
            .thenReturn(false).thenReturn(false).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(lookupGlobalMediator.getListMyEvent(anyString(), any(User.class)))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        lookupGlobalListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(3)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }


    @Test
    void testListenerIsRespondingToToday() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!today");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString()))
            .thenReturn(false).thenReturn(false).thenReturn(false)
            .thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(lookupGlobalMediator.getListEvent(anyString(), any(User.class)))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        lookupGlobalListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(4)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }

    @Test
    void testListenerIsRespondingToTmrw() {
        MessageReceivedEvent messageEvent = mock(MessageReceivedEvent.class);
        Message message = mock(ReceivedMessage.class);
        User author = mock(User.class);
        MessageChannel channel = mock(MessageChannel.class);

        when(messageEvent.getMessage()).thenReturn(message);
        when(message.getContentRaw()).thenReturn("!tmrw");
        when(message.getAuthor()).thenReturn(author);
        when(messageEvent.getChannel()).thenReturn(channel);

        when(pacilmateUtility.isQuery(anyString(), anyString()))
            .thenReturn(false).thenReturn(false).thenReturn(false)
            .thenReturn(false).thenReturn(true);

        Message responseMessage = mock(Message.class);
        MessageAction afterSend = mock(MessageAction.class);

        when(lookupGlobalMediator.getListEvent(anyString(), any(User.class)))
            .thenReturn(responseMessage);
        when(channel.sendMessage(any(Message.class))).thenReturn(afterSend);

        doNothing().when(afterSend).queue();

        lookupGlobalListener.onMessageReceived(messageEvent);

        verify(messageEvent, times(1)).getMessage();
        verify(message, times(1)).getContentRaw();
        verify(message, times(1)).getAuthor();
        verify(messageEvent, times(1)).getChannel();
        verify(pacilmateUtility, times(5)).isQuery(any(), any());
        verify(channel, times(1)).sendMessage(any(Message.class));
        verify(afterSend, times(1)).queue();
    }
}
