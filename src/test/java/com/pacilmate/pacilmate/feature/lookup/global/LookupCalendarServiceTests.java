package com.pacilmate.pacilmate.feature.lookup.global;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class LookupCalendarServiceTests {

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @InjectMocks
    LookupCalendarServiceImpl lookupCalendarService;

    PacilmateUser pacilmateUser = mock(PacilmateUser.class);
    PacilmateCalendar pacilmateCalendar = mock(PacilmateCalendar.class);


    @BeforeAll
    void setupAll() {
        when(pacilmateCalendar.getDescription()).thenReturn("test");
        when(pacilmateCalendar.getCalendarId()).thenReturn(1);
    }

    @Test
    void testGetListSubscribedCalendarServiceOK() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);
        when(pacilmateUser.getSubscribedCalendar()).thenReturn(calendars);

        assertEquals(calendars, lookupCalendarService.getListSubscribedCalendar(123123123L));
    }

    @Test
    void testGetListMyCalendarServiceOK() {
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(pacilmateUser);

        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);
        when(pacilmateUser.getMadeCalendar()).thenReturn(calendars);

        assertEquals(calendars, lookupCalendarService.getListMyCalendar(123123123L));
    }
}
