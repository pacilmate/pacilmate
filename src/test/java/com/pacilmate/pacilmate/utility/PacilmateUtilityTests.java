package com.pacilmate.pacilmate.utility;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.exception.PacilmateUnverifiedCommandException;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.repository.PacilmateUserRepository;
import java.util.List;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PacilmateUtilityTests {

    @Mock
    PacilmateUserRepository pacilmateUserRepository;

    @InjectMocks
    PacilmateUtilityImpl pacilmateUtility;

    @Test
    void testQueryCheck() {
        assertTrue(pacilmateUtility.isQuery("!makecalasdasd", "makecal"));
        assertTrue(pacilmateUtility.isQuery("!makecal asdasd", "makecal"));
        assertFalse(pacilmateUtility.isQuery("!makecal asdasd", "makeev"));
    }

    @Test
    void testRandomColor() {
        assertEquals(7, pacilmateUtility.randomColor().length());
        assertTrue(pacilmateUtility.randomColor().startsWith("#"));
        String randomColor = pacilmateUtility.randomColor();
        assertEquals(7, randomColor.length());
        for (int i = 1; i < 7; i++) {
            assertTrue(('0' <= randomColor.charAt(i) && randomColor.charAt(i) <= '9')
                || ('a' <= randomColor.charAt(i) && randomColor.charAt(i) <= 'f'));
        }
    }

    @Test
    void testVerifyMatchOK() throws PacilmateUnverifiedCommandException {
        String query = "!makecal, test, venti, hu ha, hu ha";
        User author = mock(User.class);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(mock(PacilmateUser.class));
        when(author.getIdLong()).thenReturn(123123123L);

        List<String> splitted = pacilmateUtility.verify(author, query, 4);
        assertEquals(4, splitted.size());
        assertEquals("hu ha", splitted.get(3));
        assertEquals("venti", splitted.get(1));
    }

    @Test
    void testVerifyMatchCountThrowException() throws PacilmateUnverifiedCommandException {
        String query = "!makecal, test, venti, hu ha, hu ha";
        User author = mock(User.class);

        assertThrows(PacilmateUnverifiedCommandException.class, () -> {
            pacilmateUtility.verify(author, query, 3);
        });
    }

    @Test
    void testVerifyMatchUserInDb() throws PacilmateUnverifiedCommandException {
        String query = "!makecal, test, venti, hu ha, hu ha";
        User author = mock(User.class);
        when(pacilmateUserRepository.findByUserId(anyLong())).thenReturn(null);
        when(author.getIdLong()).thenReturn(123123123L);
        when(pacilmateUserRepository.save(any())).thenReturn(null);

        List<String> splitted = pacilmateUtility.verify(author, query, 4);
        assertEquals(4, splitted.size());
        assertEquals("hu ha", splitted.get(3));
        assertEquals("venti", splitted.get(1));
    }
}
