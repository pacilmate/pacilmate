package com.pacilmate.pacilmate.utility.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import com.pacilmate.pacilmate.model.PacilmateCalendar;
import com.pacilmate.pacilmate.model.PacilmateEvent;
import com.pacilmate.pacilmate.model.PacilmateUser;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalCalendarEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateGlobalEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.embed.PacilmateManageEventEmbedBuilder;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateCalendarMessageBuilder;
import com.pacilmate.pacilmate.utility.builder.message.PacilmateEventMessageBuilder;
import com.pacilmate.pacilmate.utility.datetime.PacilmateDateTimeFormatter;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PacilmateUtilityBuilderTests {

    @Mock
    PacilmateEventMessageBuilder pacilmateEventMessageBuilderMock;

    @Mock
    PacilmateCalendarMessageBuilder pacilmateCalendarMessageBuilderMock;

    @Spy
    PacilmateDateTimeFormatter pacilmateDateTimeFormatter;

    @InjectMocks
    PacilmateCalendarEmbedBuilder pacilmateCalendarEmbedBuilder;

    @InjectMocks
    PacilmateEventEmbedBuilder pacilmateEventEmbedBuilder;

    @InjectMocks
    PacilmateCalendarMessageBuilder pacilmateCalendarMessageBuilder;

    @InjectMocks
    PacilmateManageEventEmbedBuilder pacilmateManageEventEmbedBuilder;

    @InjectMocks
    PacilmateEventMessageBuilder pacilmateEventMessageBuilder;

    @InjectMocks
    PacilmateGlobalCalendarEmbedBuilder globalCalendarEmbedBuilder;

    @InjectMocks
    PacilmateGlobalEventEmbedBuilder globalEventEmbedBuilder;

    PacilmateEvent pacilmateEvent;
    PacilmateCalendar pacilmateCalendar;
    OffsetDateTime eventTime;
    PacilmateUser pacilmateUser;
    User mockUser = mock(User.class);

    @BeforeAll
    void setupAll() {
        mockUser = mock(User.class);
        when(mockUser.getIdLong()).thenReturn(123123123L);
        when(mockUser.getName()).thenReturn("Venti hu ha");
        when(mockUser.getDiscriminator()).thenReturn("venti#3333");
        when(mockUser.getEffectiveAvatarUrl()).thenReturn("https://google.com");

        pacilmateUser = new PacilmateUser(mockUser);

        eventTime = OffsetDateTime.now(ZoneId.of("+7")).plusHours(4);

        pacilmateCalendar =
            new PacilmateCalendar("testCalendar", pacilmateUser, "#3e9f85");

        pacilmateEvent =
            new PacilmateEvent("testEvent", eventTime, pacilmateUser, "#3e9f85");

        ArrayList<PacilmateEvent> eventList = new ArrayList<>();
        eventList.add(pacilmateEvent);
        pacilmateCalendar.setEventList(eventList);
    }

    @Test
    void makeCalendarEmbedBuilderWorksWell() {
        MessageEmbed messageEmbed = pacilmateCalendarEmbedBuilder.makeEmbed(pacilmateCalendar);
        assertEquals(pacilmateCalendar.getDescription(), messageEmbed.getTitle());
    }

    @Test
    void makeEventEmbedBuilderWorksWell() {
        MessageEmbed messageEmbed = pacilmateEventEmbedBuilder.makeEmbed(pacilmateEvent);
        assertEquals(pacilmateEvent.getDescription(), messageEmbed.getTitle());
    }

    @Test
    void makeCalendarMessageBuilderWorksWell() {
        String message = pacilmateCalendarMessageBuilder.makeMessage(pacilmateCalendar);
        assertTrue(message.contains("testCalendar"));
    }

    @Test
    void makeEventMessageBuilderWorksWell() {
        String message = pacilmateEventMessageBuilder.makeMessage(pacilmateEvent);
        assertTrue(message.contains("testEvent"));
    }

    @Test
    void testGlobalCalendarEmbedBuilderWorksWell() {
        List<PacilmateCalendar> calendars = new ArrayList<>();
        calendars.add(pacilmateCalendar);
        when(pacilmateCalendarMessageBuilderMock
                .makeMessage(any(PacilmateCalendar.class)))
                .thenReturn(pacilmateCalendar.getDescription());
        globalCalendarEmbedBuilder.setAuthor(pacilmateUser);
        MessageEmbed messageEmbed = globalCalendarEmbedBuilder.makeEmbed(calendars);
        assertTrue(messageEmbed.getDescription()
                .contains(pacilmateCalendar.getDescription()));
    }

    @Test
    void testGlobalEventEmbedBuilderWorksWell() {
        List<PacilmateEvent> events = new ArrayList<>();
        events.add(pacilmateEvent);
        when(pacilmateEventMessageBuilderMock
                .makeMessage(any(PacilmateEvent.class)))
                .thenReturn(pacilmateEvent.getDescription());
        globalEventEmbedBuilder.setAuthor(pacilmateUser);
        MessageEmbed messageEmbed = globalEventEmbedBuilder.makeEmbed(events);
        System.out.println(messageEmbed.getDescription());
        assertTrue(messageEmbed.getDescription()
                .contains(pacilmateEvent.getDescription()));
    }

    @Test
    void manageEventEmbedBuilderWorksWell() {
        MessageEmbed messageEmbed = pacilmateManageEventEmbedBuilder.makeEmbed(pacilmateCalendar);
        assertTrue(
            Objects.requireNonNull(Objects.requireNonNull(messageEmbed.getAuthor()).getName())
                .contains("Venti hu ha"));
    }
}
