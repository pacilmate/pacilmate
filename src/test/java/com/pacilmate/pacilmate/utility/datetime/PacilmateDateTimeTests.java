package com.pacilmate.pacilmate.utility.datetime;

import static org.junit.jupiter.api.Assertions.*;

import com.pacilmate.pacilmate.exception.PacilmateParseDateTimeException;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PacilmateDateTimeTests {

    @InjectMocks
    PacilmateDateTimeParserImpl pacilmateDateTimeParser;

    @InjectMocks
    PacilmateDateTimeFormatterImpl pacilmateDateTimeFormatter;

    @BeforeEach
    void setup() {
        pacilmateDateTimeParser.init();
    }

    @Test
    void testParseTime() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("18:40", "today");
        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).getDayOfMonth(), result.getDayOfMonth());
        assertEquals(40, result.getMinute());
        assertEquals(18, result.getHour());
    }


    @Test
    void testParseTimeFailedCompilations() throws PacilmateParseDateTimeException {
        pacilmateDateTimeParser.init();
        assertThrows(PacilmateParseDateTimeException.class, () -> {
            pacilmateDateTimeParser.parse("asd", "today");
        });
        assertThrows(PacilmateParseDateTimeException.class, () -> {
            pacilmateDateTimeParser.parse("18:40", "asd");
        });
    }

    @Test
    void testPacilmateDateTimeFormatterIsGood() {
        OffsetDateTime currentTime = OffsetDateTime.now(ZoneId.of("+7"));
        String result = pacilmateDateTimeFormatter.getDateTime(currentTime);
        assertTrue(result.contains(String.valueOf(currentTime.getYear())));
    }

    @Test
    void testParseTimeHours() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("3h", "today");

        assertTrue(Duration.between(OffsetDateTime.now(ZoneId.of("+7")),
            result).minusHours(3).getSeconds() <= 10);
    }

    @Test
    void testParseTimeMinutes() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("3m", "today");

        assertTrue(Duration.between(OffsetDateTime.now(ZoneId.of("+7")),
            result).minusMinutes(3).getSeconds() <= 10);
    }

    @Test
    void testParseTimePlusException() throws PacilmateParseDateTimeException {
        assertThrows(PacilmateParseDateTimeException.class, () -> {
            pacilmateDateTimeParser.parse("3asdasdm", "today");
        });
        assertThrows(PacilmateParseDateTimeException.class, () -> {
            pacilmateDateTimeParser.parse("3asdasdh", "today");
        });
    }

    @Test
    void testParseExactDateHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("00:00", "24-08-2002");

        assertEquals(24, result.getDayOfMonth());

    }

    @Test
    void testParsePlusDateHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("00:00", "5");

        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).plusDays(5).getDayOfMonth(),
            result.getDayOfMonth());

    }

    @Test
    void testParseTomorrowHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("00:00", "tomorrow");

        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).plusDays(1).getDayOfMonth(),
            result.getDayOfMonth());

    }

    @Test
    void testParseNextWeekHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("00:00", "next week");

        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).plusWeeks(1).getDayOfMonth(),
            result.getDayOfMonth());

    }

    @Test
    void testParseNextYearHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("00:00", "next year");

        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).plusYears(1).getDayOfMonth(),
            result.getDayOfMonth());

    }

    @Test
    void testParseNextMonthHandler() throws PacilmateParseDateTimeException {
        OffsetDateTime result = pacilmateDateTimeParser.parse("18:30", "next month");

        assertEquals(OffsetDateTime.now(ZoneId.of("+7")).plusMonths(1).getDayOfMonth(),
            result.getDayOfMonth());

    }
}
