package com.pacilmate.pacilmate;

import static org.junit.jupiter.api.Assertions.assertNull;

import com.pacilmate.pacilmate.jda.PacilmateJdaServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PacilmateApplicationTests {

    @Spy
    PacilmateJdaServiceImpl pacilmateService;

    @Test
    void contextLoads() {
        assertNull(pacilmateService.getjda());
    }

}
