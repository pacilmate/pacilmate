# PacilMate

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/master/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/master)
[![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/master/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/master)

[![PacilMate](https://img.shields.io/badge/PacilMate%20Main%20Service-7289DA?style=flat&logo=discord&logoColor=white)](https://gitlab.com/pacilmate/pacilmate/)
[![PacilJudge](https://img.shields.io/badge/PacilJudge%20Micro%20Service-FF7373?style=flat&logo=discord&logoColor=white)](https://gitlab.com/pacilmate/paciljudge/)
[![Testing Guide](https://img.shields.io/badge/-Testing%20Guide%20@Wiki-blue)](https://gitlab.com/pacilmate/pacilmate/-/wikis/Testing-Guide)

PacilMate is a Work-in-Progress **Discord Bot** made with love by our developers!

- Aditya Pratama - 1706039490
- Farah Nazihah - 1906350761
- Hocky Yudhiono - 1906285604
- Muhammad Urwatil Wutsqo - 1906351101
- Wiena Amanda - 1806186591

It is a project made to fulfill the **CSCM602223 - Advanced Programming** course.

## Features Coverage
**Global Lookup** - Farah

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/farah-coverage/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/farah-coverage) [![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/farah-coverage/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/farah-coverage)

**Calendar Lookup** - Wiena

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/wiena-coverage/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/wiena-coverage) [![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/wiena-coverage/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/wiena-coverage)

**Builder** - Hocky

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/hocky-coverage/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/hocky-coverage) [![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/hocky-coverage/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/hocky-coverage)

**Manage Calendar** - Wutsqo

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/wutsqo-coverage/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/wutsqo-coverage) [![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/wutsqo-coverage/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/wutsqo-coverage)

**Manage Event** - Aditya

[![pipeline status](https://gitlab.com/pacilmate/pacilmate/badges/aditya-coverage/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/aditya-coverage) [![coverage report](https://gitlab.com/pacilmate/pacilmate/badges/aditya-coverage/coverage.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/aditya-coverage)


## Bot Feature

We use `!` as the bot prefix. It's easy to customize it, but we know it won't be such a big problem.
So we will implement it as additional feature on the future. Also, you'll use `,` to separate stuffs.
Currently, our mapped features are as follows:

:heart: **Global Lookup**

`!listcal`: List all of your subscribed calendars.  
`!mycal`: List all of your made calendars.  
`!myev`: List all of your made events.  
`!today`: List today's events.  
`!tmrw`: List tomorrow's events.  

🧡 **Query Lookup**

`!ev, {number}`: List the first {number} future events.  
`!evdate, {date_parse}`: List events specified in the {date_parse}.  

:yellow_heart: **Builder**

`!makecal, {title}`: Create a calendar.  
`!makeev, {description}, {time_parse}, {date_parse}`: Create an event.  

:green_heart: **Edit**

`!editcal, {calendar_id}, {title}`: Edit calendar.  
`!editev, {event_id}, {description}, {time_parse}, {date_parse}`: Edit event.  
`!delcal, {calendar_id}`: Delete calendar.  
`!delev, {event_id}`: Delete event.  

:blue_heart: **Manage Calendar**

`!sub, {calendar_id}`: Subscribe calendar with specified id.  
`!unsub, {calendar_id}`: Unsubscribe calendar with specified id.  

:purple_heart: **Manage Events**

`!set, {calendar_id}, {event_id}`: Add event to your made calendar.  
`!unset, {calendar_id}, {event_id}`: Remove event to your made calendar.  

🤎 **Judge**

`!problems`: Get all problems title.  
`!init`: Initialize yourself into **PacilJudge**.
`!solve, {problem_id}`: Try to solve this question.  
`!release`: Surrender answering this question.  
`!answer, {answer}`: Answer this question.  
`!score`: Check my score.  
`!scoreboard`: Check scoreboard.  

